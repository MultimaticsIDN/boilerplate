var dateMin= null;
var dateMax = null;
function applyAutoComplete(id,url,message,key){
    $( "#"+id).autocomplete({
        source: BaseURL+url,
        //minLength: 0,
        selectFirst: true, //here
        select: function( event, ui ) { 
            var target = $(this).attr("target");
            $("#"+target).val(ui.item.id); 
            if(key == "aircraft_registration"){
                getDataAircraft(ui.item.id);
            }else if(key == "aircraft_name"){
                getDataAircraftType(ui.item.id);
            }else if(key == "operator_name"){

            }else if(key == "company_name"){
                getDataCompany(ui.item.id);
            }
            // getDataPassenger(ui.item.id, count1);
        },
         change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $("#"+message).show();
            } else {
                $("#"+message).hide();
            }
        },
    });
}

function getDataAircraft(id){
    getMinMaxDate(id);
    $.get(BaseURL+"api/aircraft/"+id,function(aircraft)  {
         getDataAircraftType(aircraft.data.aircraft_type);
        $("#serial_number").val(aircraft.data.serial_number);
        $("#production_year").val(aircraft.data.production_year);
        $("#total_number_of_seat").val(aircraft.data.total_number_of_seat);
        $("#owner").val(aircraft.data.owner);
        $("#trustee").val(aircraft.data.trustee);  
    });
}

function getDataAircraftType(id){
    $.get(BaseURL+"api/aircraft_types/"+id,function(aircraftType)  {
        $("#mtow").val(aircraftType.data.mtow); 
        $("#aircraft_name").val(aircraftType.data.aircraft_name); 
        $("#id_aircraft_type").val(aircraftType.data.id_aircraft_type); 
    });
}

function getDataCompany(id){
    console.log(id);
    $.get(BaseURL+"api/companies/"+id,function(company)  {
        $("#address").val(company.data.address); 
        $("#attn_to").val(company.data.attn_to); 
        $("#attn_pos").val(company.data.attn_pos); 
        $("#email").val(company.data.email); 
        $("#contacts").val(company.data.contacts);  
    });
}

function getMinMaxDate(id){
    console.log(id);
    $.get(BaseURL+"api/flight_contract/getMinMaxDate/"+id,function(MaxMin)  { 
        dateMin = MaxMin.data.Min;
        dateMax = MaxMin.data.Max;
    });
}

function setCurrency(value){
    console.log(value);
    if(value == 64){
        $(".label_currency").html("IDR");
    }else{
        $(".label_currency").html("USD");
    }
    
}

function cekBetweenDate(date){
    if(dateMin != null && dateMax != null){
        if(dateMin <= date && date <= dateMax){
            alert("Flight contracts already exist on a date range " +dateMin +" - "+dateMax);
         }
    }   
}

function saveCompanyOperator(role){
    var relation = "";
    var data = {};
    if(role == "operator_name"){ 
        relation = "operator";
        data = {
            name : $('#name_operator').val(),
            address : $('#address_operator').val(),
            email : $('#email_operator').val(), 
            contacts : $('#contacts_operator').val(), 
            relation : relation,  
            attn_pos : $('#attn_pos_operator').val(),   
            attn_to : $('#attn_to_operator').val(), 
        }; 
    }
    if(role == "company_name"){ 
        relation = "customer";
        data = {
            name : $('#name_company').val(),
            address : $('#address_company').val(),
            email : $('#email_company').val(), 
            contacts : $('#contacts_company').val(), 
            relation : relation,  
            attn_pos : $('#attn_pos_company').val(),   
            attn_to : $('#attn_to_company').val(), 
        }; 
    }
    $.post(BaseURL + "api/companies", data)
    .done(function (output) { 
        console.log(output);
        if(role == "operator_name"){
            addDataCombobox(
                role,
                $('#name_operator').val(),
                output.data.company_id
            );
        }else if(role == "company_name"){
            addDataCombobox(
                role,
                $('#name_company').val(),
                output.data.company_id
            );
             getDataCompany(output.data.company_id);
        }
        alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

function addDataCombobox(idSelect,dataText,dataValue) {
  var x = document.getElementById(idSelect);
  var option = document.createElement("option");
  option.text = dataText;
  option.value = dataValue;
  option.selected = "selected";
  x.add(option); 
}

function sumPersenSerice(data,persen,total) {
    var target = $(data).attr("target");  
    var max = parseInt($("#"+persen).attr('max')); 
    if (replaceKoma($("#"+persen).val()) > max)
       { 
        alert("Please fill in the numbers 1 to 100");
        if ($("#"+persen).is(":focus")) {
            $("#"+persen).val('');
            $("#"+total).val(formatMoney(0)); 
        }
    } else{
        if(!isBlank(target)){ 
            var isi = 0; 
           if(!isBlank(replaceKoma($("."+target).val()))){
                isi = replaceKoma($("."+target).val());
           }
           var isiPersen = 0;
           if(!isBlank($("#"+persen).val())){
                isiPersen = $("#"+persen).val();
           } 
           var ttl = isi*isiPersen/100;
           $("#"+total).val(formatMoney(ttl)); 
       }
    }   
    
   
}

function sumTotalPersenSerice(data,total,harga) {
   var target = $(data).attr("target"); 
   var isi = 0;
   var totalHarga = [];
   $("."+target).each(function(){
           var isiPersen = replaceKoma($(this).val())
           if (!isNaN(isiPersen)) {
                isi = replaceKoma($("#"+harga).val());
                var ttl = isi*isiPersen/100;
                console.log(isi);
                totalHarga.push(ttl);
           }          
    })    

    $("."+total).each(function(key, val){
      var idTotal = $(this).attr('id');
      $("#"+idTotal).val(formatMoney(totalHarga[key]));
    });

}

function cekDoubleInputPrice(isPersen,keyPersen,persen,price){
    if(isPersen == 1 && isBlank(keyPersen)){ 
        if(!isBlank($("#"+persen).val()) && parseInt(replaceKoma($('#'+persen).val())) != 0 && !isBlank($("#"+price).val()) && parseInt(replaceKoma($('#'+price).val())) != 0){
            alert("Please fill in one of the fields");
            if ($("#"+persen).is(":focus")) {
                $("#"+persen).val('');
            }
            if ($("#"+price).is(":focus")) {
                $("#"+price).val('');
            }
        }
    }
}

$( document ).ready(function() {
    $('#operator_name').select2({
        placeholder: 'Select a operator'
    });

     $('#company_name').select2({
        placeholder: 'Select a company'
    });

     $('#aircraft_registration').select2({
        placeholder: 'Select a aircraft'
    });
    $("input").bind("keydown", function(event) {
        var code = event.keyCode || event.which;
        var inputs = $(this).parents("form").eq(0).find(":input:not([type=hidden])"); 
        var idx = inputs.index(this);
        // event.preventDefault();
        if (code == 13 || code == 40) { // arrow down and enter code 
                    /* FOCUS ELEMENT */
                    if (idx == inputs.length - 1) {
                        inputs[0].select()     
                    } else {
                        inputs[idx + 1].focus(); //  handles submit buttons
                        // inputs[idx + 1].select();
                    }
                     return false;
        }
        else if(code == 38){ // key up or arrow up code
                    /* FOCUS ELEMENT */
                    if (idx == inputs.length - 1) {
                        inputs[0].select()     
                    } else {
                        inputs[idx - 1].focus(); //  handles submit buttons
                        // inputs[idx + 1].select();
                    }
                    return false;
        }
    });


});
