$(document).ready(function() {
    $('#flightContracts-table').DataTable({
        // "scrollX": true
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: window.location.pathname
            },
            {
                extend: 'excelHtml5',
                title: window.location.pathname
            },
            {
                extend: 'pdfHtml5',
                title: window.location.pathname
            }
         
        ],
    });
} );
