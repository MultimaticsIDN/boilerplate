$(document).ready(function() {
    $('#airports-table').DataTable({
        scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"airports/getJson",
        columns: [
            {data: 'city', name: 'city.name'},
            {data: 'airport_name', name: 'airport_name'},
            {data: 'icao_code', name: 'icao_code'},
            {data: 'iata_code', name: 'iata_code'},
            {data: 'pcn', name: 'pcn'},
            {data: 'runway_length', name: 'runway_length'},
            {data: 'operation_time', name: 'operation_time'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );