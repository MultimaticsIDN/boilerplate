$(document).ready(function() {
    $('#users-table').DataTable({
        // scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"users/getJson",
        columns: [
            // {data: 'currency_id', name: 'currency_id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'roles_name', name: 'roles_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );