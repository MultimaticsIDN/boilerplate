$(document).ready(function() {
    $('#continents-table').DataTable({
        scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"continent/getJson",
        columns: [
            {data: 'name', name: 'name'},  
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );
