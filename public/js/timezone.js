$(document).ready(function() {
    $('#timezones-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: BaseURL+"timezones/getJson",
        columns: [
            // {data: 'currency_id', name: 'currency_id'},
            {data: 'name', name: 'name'},
            {data: 'gmt', name: 'gmt'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );