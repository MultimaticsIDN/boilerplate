$(document).ready(function() {
  $('#flightContracts-table').DataTable({
      scrollX : true,
      processing: true,
      serverSide: true,
      dom: 'Bfrtip',
      ajax: BaseURL+"pricelist/getJson",
      columns: [
          {data: 'aircraft_registration', name: 'aircraft.aircraft_registration'},
          {data: 'aircraft_name', name: 'AT.aircraft_name'},
          {data: 'operator_name', name: 'operator.name'},
          {data: 'company_name', name: 'company.name'},  
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ],
      buttons: [
          {
              extend: 'csvHtml5',
              title: window.location.pathname
          },
          {
              extend: 'excelHtml5',
              title: window.location.pathname
          },
          {
              extend: 'pdfHtml5',
              title: window.location.pathname
          }
       
      ],
  });
} );