$(document).ready(function() {
    $('#currencies-table').DataTable({
        // scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"currencies/getJson",
        columns: [
            // {data: 'currency_id', name: 'currency_id'},
            {data: 'currency_name', name: 'currency_name'},
            {data: 'currency_code', name: 'currency_code'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );