$(document).ready(function() {
    $('#visaTypes-table').DataTable({
    	scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"visaType/getJson",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'requirements', name: 'requirements'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );