$(document).ready(function() {
    $('#companies-table').DataTable({
        // scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+flagsUrl+"/getJson",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'contacts', name: 'contacts'},
            {data: 'address', name: 'address'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );