$(document).ready(function() {
    $('#flightServiceCategories-table').DataTable({
    	scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"flightServiceCategory/getJson",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'invoice_prefix', name: 'invoice_prefix'},  
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );