var counterRoute = $('#countRoute').val();
var counterCrew = $('#countCrew').val();
var counterPassenger = $('#countPassenger').val();
var timeZoneETA = null;
var timeZoneETD = null; 
var dataPassenger = [];
var dataCrew = [];
var dataHandler = [];
var dataHandlerTo = [];
var dataAirport = [];

function applyAutoCompleteFC(displayID,endPoint,message){
   $( "#"+displayID ).autocomplete({
        source: BaseURL+endPoint,
        selectFirst: true, //here
        select: function( event, ui ) {
            var target = $(this).attr("target");
            console.log(target);
            $("#"+target).val(ui.item.id);
            getDataCompany(ui.item.value_operator);
        },
         change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $('#'+message).show();
            } else {
                $('#'+message).hide();
            }
        },
    });
}

function showDate(getdate,count) {
	var selectedDate = new Date(getdate);
	var dateformat = getdate.split('-'); 
	var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
 
	$('#'+count).val(days[selectedDate.getDay()]); 
}

function getDataCompany(data){
	var id = $(data).find('option:selected').attr("name");
	 $("#operator_name").select2().val(id).trigger('change.select2'); 
    // $.get(BaseURL+"api/companies/"+id,function(company)  {
    	// $("#operator_name").val( id);
    	// $("#operator_name option[value='" + id +"']").attr("selected","selected");
        // $("#operator_name").val(company.data.name); 
        // $("#operator_id").val(company.data.company_id);  
    // });
}

function addRoute(count){
	var row = "<tr>";
 		row +=     "<td  class='numberLeg'>";
		row +="        <center>"+ count +"</center>";
		row +="      </td>";
		row +="      <td>";
		row +="        <a href='#' class='btn btn-danger' onclick='removeRow(this)'>Delete</a>";
		row +="      </td>";
		row +="      <td>";
		row +="        <input type='text' id='arrive_from_name"+ count +"' target1='depart_zone"+ count +"'  name='arrive_from_name[]' class='form-control' target='arrive_from"+ count +"' onkeyup='autoArrive()' onblur='getTimezoneETD(this)'>";
		row +="        <input type='hidden' name='arrive_from[]' id='arrive_from"+ count +"' class='form-control'>";
		row +="         <div id='arrive_from_name"+ count +"-message' class='alert alert-danger' role='alert' style='margin-top:5px; display:none;'>";
		row +="            Please select item !";
		row +="          </div>";
		row +="  		<input type='hidden' class='form-control' value=''  name='flight_leg_id[]'>"; 
        row +="         <input type='hidden' name='depart_zone[]' id='depart_zone"+ count +"' class='form-control'>";
		// row +="		<select id='arrive_from"+ count +"' name='arrive_from[]' class='arrive_from form-control' onchange='getTimezoneETD(this.value)''>";
		// row +="			 <option value='' readonly>Select a from</option>";
		// row +="		</select>";
		row +="      </td>";
		row +="      <td>";
		row +="        <input type='text' id='departed_for_name"+ count +"' target1='arrive_zone"+ count +"'  name='departed_for_name[]' class='form-control departed_for_name_1' target='departed_for"+ count +"' onkeyup='autoDepart()' onblur='getTimezoneETA(this)'>";
		row +="         <input type='hidden' name='departed_for[]' id='departed_for"+ count +"' class='form-control'>";
		row +="         <div id='departed_for_name"+ count +"-message' class='alert alert-danger' role='alert' style='margin-top:5px; display:none;'>";
		row +="            Please select item !";
		row +="          </div>";
		row +="         <input type='hidden' name='arrive_zone[]' id='arrive_zone"+ count +"' class='form-control'>";
		// row +="		<select id='departed_for"+ count +"' name='departed_for[]' class='departed_for form-control' onchange='getTimezoneETA(this.value)''>";
		// row +="			 <option value='' readonly>Select a from</option>";
		// row +="		</select>";
		row +="      </td>";
		row +="	     <td>";
	    row +="           ZT:<br>";
	    row +="           LT:";
	    row +="      </td>";
		row +="      <td>";
		row +="        <input type='date' id='depart_at_date"+ count +"' target='depart_lt_date"+ count +"' target2='depart_at"+ count +"' target3='depart_lt"+ count +"' target4='depart_zone"+ count +"' name='depart_at_date[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy' class='tm form-control' onchange='convertDateETD(this)' required>";
		row +="        <input type='date' id='depart_lt_date"+ count +"' target='depart_at_date"+ count +"' target2='depart_lt"+ count +"' target3='depart_at"+ count +"' target4='depart_zone"+ count +"' name='depart_lt_date[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy' class='tm form-control' onchange='convertDateZuluETD(this)' required>";
		row +="      </td>";

		row +="      <td>";
		row +="        <input type='time' id='depart_at"+ count +"' target='depart_lt"+ count +"' target2='depart_at_date"+ count +"' target3='depart_lt_date"+ count +"' target4='depart_zone"+ count +"' name='depart_at[]' class='form-control' onchange='convertTimeETD(this)' required>";
		row +="        <input type='time' id='depart_lt"+ count +"' target='depart_at"+ count +"' target2='depart_lt_date"+ count +"' target3='depart_at_date"+ count +"' target4='depart_zone"+ count +"' name='depart_lt[]' class='form-control' onchange='convertTimeZuluETD(this)' required>";
		row +="      </td>"; 

		row +="      <td>";
		row +="        <input type='date' id='arrive_at_date"+ count +"' target='arrive_lt_date"+ count +"' target2='arrive_at"+ count +"' target3='arrive_lt"+ count +"' target4='arrive_zone"+ count +"' name='arrive_at_date[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy' class='tm form-control' onchange='convertDateETA(this)' required>";
		row +="        <input type='date' id='arrive_lt_date"+ count +"' target='arrive_at_date"+ count +"' target2='arrive_lt"+ count +"' target3='arrive_at"+ count +"' target4='arrive_zone"+ count +"' name='arrive_lt_date[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy' class='tm form-control' onchange='convertDateZuluETA(this)' required>";
		row +="      </td>";

		row +="       <td>";
		row +="        <input type='time' id='arrive_at"+ count +"' target='arrive_lt"+ count +"' target2='arrive_at_date"+ count +"' target3='arrive_lt_date"+ count +"' target4='arrive_zone"+ count +"' name='arrive_at[]' class='form-control' onchange='convertTimeETA(this)' required>";
		row +="        <input type='time' id='arrive_lt"+ count +"' target='arrive_at"+ count +"' target2='arrive_lt_date"+ count +"' target3='arrive_at_date"+ count +"' target4='arrive_zone"+ count +"' name='arrive_lt[]' class='form-control' onchange='convertTimeZuluETA(this)' required>";
		row +="      </td>";

		row +="      <td>";
		// row +="        <input type='text' id='company_name"+ count +"' name='company_name[]' class='form-control' onkeyup='autoCompany()' target='company_id"+ count +"' >";
		// row +="         <input type='hidden' id='company_id"+ count +"' name='company_id[]' class='form-control'>";
		// row +="         <div id='company_name"+ count +"-message' class='alert alert-danger' role='alert' style='margin-top:5px; display:none;'>";
		// row +="            Please select item !";
		// row +="          </div>";
		row +="		<select id='company_id"+ count +"' name='company_id[]' class='company_id form-control'>";
		row +="			 <option value='' readonly>Select a handler</option>";
		row +="		</select>";

		row +="      </td>";


		row +="      <td>"; 
		row +="		<select id='company_to_id"+ count +"' name='company_to_id[]' class='company_to_id form-control'>";
		row +="			 <option value='' readonly>Select a handler</option>";
		row +="		</select>"; 
		row +="      </td>";

		row +="    </tr>";
		return $("#tblRoute").append(row);
}

function addCrew(count){
	var row ="<tr>";
		row +="		<td  class='numberCrew'>";
		row +="		  <center>"+count+"</center>  ";
		row +="		</td>";
		row +="		<td>"
		row +="		  <a href='#' class='btn btn-danger' onclick='removeRow(this,counterCrew--)'>Delete</a>";
		row +="		</td>";
		row +="		<td>";
		// row +="		  <input type='text' name='codenameCrew[]' id='codenameCrew"+ count +"' class='form-control crew_1' target='crew_id"+ count +"' onkeyup='autoCrew()'>";
		// row +="		 <input type='hidden' name='crew_id[]' id='crew_id"+ count +"' class='form-control'>";
		// row +="		 <div id='codenameCrew"+ count +"-message' class='alert alert-danger' role='alert' style='margin-top:5px; display:none;'>"
		// row +="		    Please select item !"
		// row +="		  </div>"
		row +="		<select id='crew_id"+ count +"' name='crew_id[]' class='crew_id form-control crew_1' style='width: 300px'>";
		row +="			 <option value='' readonly>Select a crew</option>";
		row +="		</select>";
		row +="		</td>";
		row +="		</tr>";
		return $("#tblCrew").append(row);
}

function addPassenger(count){ 
var row =" <tr>";
	row +="    <td class='numberPassenger'>";
	row +="      <center>"+count+"</center>";
	row +="    </td>";
	row +="   <td>";
	row +="     <a href='#' class='btn btn-danger' onclick='removeRow(this,counterPassenger--)'>Delete</a>";
	row +="    </td>";
	row +="   <td>";
	// row +="      <input type='text' id='codenamePassenger"+ count +"' name='codenamePassenger[]' class='form-control passenger_1' target='passenger_id"+ count +"' onkeyup='autoPassengger()'>";
	// row +="    <input type='hidden' name='passenger_id[]' id='passenger_id"+ count +"' class='form-control'>";
	// row +="    <div id='codenamePassenger"+ count +"-message' class='alert alert-danger' role='alert' style='margin-top:5px; display:none;'>";
	// row +="       Please select item !";
	// row +="     </div>";
	row +="		<select id='passenger_id"+ count +"' name='passenger_id[]' class='passenger_id form-control passenger_1' style='width: 300px'>";
	row +="			        <option value='' readonly>Select a nationality</option>";
	row +="		</select>";
	row +="   </td>";
	row +="</tr>";
	return $("#tblPassenger").append(row);
}

function showDateDay(value,count){
	// var count = counterRoute;
	showDate(value,"day"+count);
}


function autoDepart(){
	applyAutoComplete("departed_for_name"+ counterRoute ,"api/airport/autocompleteByCity","departed_for_name"+ counterRoute +"-message");
} 

function autoArrive(){
	applyAutoComplete("arrive_from_name"+ counterRoute,"api/airport/autocompleteByCity","arrive_from_name"+ counterRoute +"-message");
}

function autoCompany(){
	applyAutoComplete("company_name"+ counterRoute ,"api/company/autocomplete/vendor","company_name"+ counterRoute +"-message");
}

function autoCrew(){
	applyAutoComplete("codenameCrew"+ counterCrew,"api/crew/autocomplete","codenameCrew"+ counterCrew +"-message+");
}

function autoPassengger(){
	applyAutoComplete("codenamePassenger"+ counterPassenger,"api/passenger/autocomplete","codenamePassenger"+ counterPassenger +"-message");
}

function removeRow(elem,counter) {
    $(elem).closest("tr").remove();
    // counter; 
	changeNumber("numberLeg");
	changeNumber("numberCrew");
	changeNumber("numberPassenger");
}


function getAirport(id){
	$.get(BaseURL+"api/airports/"+id,function(airport)  { 
		$('#icao_code').val(airport.data.icao_code);
		$('#iata_code').val(airport.data.iata_code);
		$('#airport_name').val(airport.data.airport_name);
		$('#pcn').val(airport.data.pcn);
		$('#latitude').val(airport.data.latitude);
		$('#longitude').val(airport.data.longitude);
		$('#runway_length').val(airport.data.runway_length);
		$('#geoname_id').val(airport.data.geoname_id);
		$('#operation_time').val(airport.data.operation_time);
		$('#phone_number').val(airport.data.phone_number); 

		$("#country_id").select2().val(airport.data.country_id).trigger('change.select2'); 
		$("#city_id").select2().val(airport.data.city_id).trigger('change.select2'); 
		$("#timezone_id").select2().val(airport.data.timezone_id).trigger('change.select2');  
		$('#mAirport').modal('show');
    });
}

function saveAirport(){
    var data = {
        iata_code : $('#iata_code').val(),
        airport_name : $('#airport_name').val(),
        pcn : $('#pcn').val(), 
        latitude : $('#latitude').val(), 
        longitude : $('#longitude').val(), 
        runway_length : $('#runway_length').val(), 
        geoname_id : $('#geoname_id').val(), 
        operation_time : $('#operation_time').val(), 
        phone_number : $('#phone_number').val(),  
        country_id : $('#country_id option:selected').val(),
        city_id : $('#city_id option:selected').val(),
        timezone_id : $('#timezone_id option:selected').val(),  
    }; 
    $.post(BaseURL + "api/airport/update/"+$('#icao_code').val(), data)
    .done(function (output) {  
    	if(isBlank($('#timezone_id option:selected').val())){
    		getAirport($('#icao_code').val());
    	}else{

			var target = $('#positionTime').attr("target"); 
    		if($('#positionTime').val() == "ETD"){
	    		timeZoneETD = $("#timezone_id option:selected").text();
	    		$('#'+target).val(timeZoneETD);
	    	}else{
	    		timeZoneETA = $("#timezone_id option:selected").text();
	    		$('#'+target).val(timeZoneETA);
	    	}
    	}
    	
    	alert("Save data successfully");   
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}


function getTimezoneETD(data){ 
	var target = $(data).attr("target"); 
	var timezone = $(data).attr("target1");  
	setTimezoneETD($("#"+target).val(),timezone);
}

function setTimezoneETD(id,timezone){
    $.get(BaseURL+"api/airport/showTimeZone/"+id,function(time)  {  
		timeZoneETD = time.data.name;
		if(isBlank(timeZoneETD)){ 
			alert("Isi timezone terlebih dahulu");
			$('#positionTime').val("ETD"); 
			$('#positionTime').attr('target', ''+timezone+'');
			getAirport(id);
		}else{
			$("#"+timezone).val(timeZoneETD);
		}
    });
}

function getTimezoneETA(data){ 
	var target = $(data).attr("target"); 
	var timezone = $(data).attr("target1"); 
	setTimezoneETA($("#"+target).val(),timezone);
}

function setTimezoneETA(id,timezone){
    $.get(BaseURL+"api/airport/showTimeZone/"+id,function(time)  { 
    	timeZoneETA = time.data.name; 
		if(isBlank(timeZoneETA)){ 
			alert("Isi timezone terlebih dahulu");
			$('#positionTime').val("ETA"); 
			$('#positionTime').attr('target', ''+timezone+'');
			getAirport(id);
		}else{
			$("#"+timezone).val(timeZoneETA);
		}
    });
}

function convertDateZuluETA(data){  
	var timezone = $(data).attr("target4");  
	console.log(timezone);
	if($("#"+timezone).val() != null){  
		moment.tz.setDefault($("#"+timezone).val());
		var target = $(data).attr("target"); 
		var targetTime1 = $(data).attr("target2");  
		var dataTime = $("#"+targetTime1).val(); 
		if(!isBlank(dataTime)){ 
		    $("#"+target).val(formatDateToYMD(toDateTimeZone(data.value+" "+dataTime, "Zulu"))); 
		    $("#"+target).attr('data-date', formatDateToMDY(toDateTimeZone(data.value+" "+dataTime, "Zulu"))); 
		}
	}
}

function convertDateZuluETD(data){  
	var timezone = $(data).attr("target4");  
	console.log(timezone);
	if($("#"+timezone).val() != null){ 
		moment.tz.setDefault($("#"+timezone).val());
		var target = $(data).attr("target");   
		var targetTime1 = $(data).attr("target2");  
		var dataTime = $("#"+targetTime1).val(); 
		if(!isBlank(dataTime)){
		    $("#"+target).val(formatDateToYMD(toDateTimeZone(data.value+" "+dataTime, "Zulu"))); 
		    $("#"+target).attr('data-date', formatDateToMDY(toDateTimeZone(data.value+" "+dataTime, "Zulu"))); 
		}
	}
}


function convertDateETA(data){  
	var timezone = $(data).attr("target4"); 
	console.log(timezone);
	if($("#"+timezone).val() != null){
		moment.tz.setDefault("Zulu");
		var target = $(data).attr("target");   
		var targetTime1 = $(data).attr("target2");  
		var dataTime = $("#"+targetTime1).val();  
		if(!isBlank(dataTime)){
		    $("#"+target).val(formatDateToYMD(toDateTimeZone(data.value+" "+dataTime, $("#"+timezone).val()))); 
		    $("#"+target).attr('data-date', formatDateToMDY(toDateTimeZone(data.value+" "+dataTime, $("#"+timezone).val())));
		} 
	}
}


function convertDateETD(data){  
	var timezone = $(data).attr("target4");
	console.log(timezone);
	if($("#"+timezone).val() != null){  
		moment.tz.setDefault("Zulu");
		var target = $(data).attr("target");   
		var targetTime1 = $(data).attr("target2"); 
		var dataTime = $("#"+targetTime1).val(); 
		if(!isBlank(dataTime)){
		    $("#"+target).val(formatDateToYMD(toDateTimeZone(data.value+" "+dataTime, $("#"+timezone).val()))); 
		    $("#"+target).attr('data-date', formatDateToMDY(toDateTimeZone(data.value+" "+dataTime, $("#"+timezone).val()))); 
		}  
	}
	
}

function convertTimeZuluETA(data){  
	var timezone = $(data).attr("target4");
	console.log(timezone);
	if($("#"+timezone).val() != null){   
		moment.tz.setDefault($("#"+timezone).val());
		var target = $(data).attr("target"); 
		var targetDate1 = $(data).attr("target2"); 
		var targetDate2 = $(data).attr("target3"); 
		var dataDate = $("#"+targetDate1).val();
		if(isBlank(dataDate)){
			var date = $("#"+targetDate1).attr('data-date'); 
			var datearray = date.split("/");
			dataDate = datearray[2] + '-' + datearray[0] + '-' + datearray[1];
		}  
	    $("#"+target).val(toTimeZone(data.value, "Zulu")); 
	    $("#"+targetDate2).val(formatDateToYMD(toDateTimeZone(dataDate+" "+data.value, "Zulu"))); 
	    $("#"+targetDate2).attr('data-date', formatDateToMDY(toDateTimeZone(dataDate+" "+data.value, "Zulu"))); 
	}
}

function convertTimeZuluETD(data){  
	var timezone = $(data).attr("target4");
	console.log(timezone);
	if($("#"+timezone).val() != null){  
		moment.tz.setDefault($("#"+timezone).val());
		var target = $(data).attr("target");   
		var targetDate1 = $(data).attr("target2"); 
		var targetDate2 = $(data).attr("target3"); 
		var dataDate = $("#"+targetDate1).val();
		if(isBlank(dataDate)){
			var date = $("#"+targetDate1).attr('data-date'); 
			var datearray = date.split("/");
			dataDate = datearray[2] + '-' + datearray[0] + '-' + datearray[1];
			console.log(dataDate);
		}  
	    $("#"+target).val(toTimeZone(data.value, "Zulu")); 
	    $("#"+targetDate2).val(formatDateToYMD(toDateTimeZone(dataDate+" "+data.value, "Zulu"))); 
	    $("#"+targetDate2).attr('data-date', formatDateToMDY(toDateTimeZone(dataDate+" "+data.value, "Zulu"))); 
	}
}

function convertTimeETA(data){  
	var timezone = $(data).attr("target4");
	console.log(timezone);
	if($("#"+timezone).val() != null){  
		moment.tz.setDefault("Zulu");
		var target = $(data).attr("target");   
		var targetDate1 = $(data).attr("target2"); 
		var targetDate2 = $(data).attr("target3"); 
		var dataDate = $("#"+targetDate1).val(); 
		if(isBlank(dataDate)){
			var date = $("#"+targetDate1).attr('data-date'); 
			var datearray = date.split("/");
			dataDate = datearray[2] + '-' + datearray[0] + '-' + datearray[1];
			console.log(dataDate);
		} 
	    $("#"+target).val(toTimeZone(data.value, $("#"+timezone).val()));  
	    $("#"+targetDate2).val(formatDateToYMD(toDateTimeZone(dataDate+" "+data.value, $("#"+timezone).val()))); 
	    $("#"+targetDate2).attr('data-date', formatDateToMDY(toDateTimeZone(dataDate+" "+data.value, $("#"+timezone).val()))); 
	}
}


function convertTimeETD(data){
	var timezone = $(data).attr("target4");
	console.log(timezone);
	if($("#"+timezone).val() != null){    
		moment.tz.setDefault("Zulu");
		var target = $(data).attr("target");  
		var targetDate1 = $(data).attr("target2"); 
		var targetDate2 = $(data).attr("target3"); 
		var dataDate = $("#"+targetDate1).val(); 
		if(isBlank(dataDate)){
			var date = $("#"+targetDate1).attr('data-date'); 
			var datearray = date.split("/");
			dataDate = datearray[2] + '-' + datearray[0] + '-' + datearray[1];
			console.log(dataDate);
		} 
	    $("#"+target).val(toTimeZone(data.value, $("#"+timezone).val()));  
	    $("#"+targetDate2).val(formatDateToYMD(toDateTimeZone(dataDate+" "+data.value, $("#"+timezone).val()))); 
	    $("#"+targetDate2).attr('data-date', formatDateToMDY(toDateTimeZone(dataDate+" "+data.value, $("#"+timezone).val()))); 
	}
	
}
function getCrew(){
    $.get(BaseURL+"api/crew/autocomplete",function(crew)  {   
    	dataCrew = crew;
        setDataCombobox("crew_id",crew); 
    });
}
function getPassenger(){
    $.get(BaseURL+"api/passenger/autocomplete",function(passenger)  {   
    	dataPassenger = passenger;
        setDataCombobox("passenger_id",passenger); 
    });
}
function getHandler(){ 
    $.get(BaseURL+"api/company/autocomplete/vendor",function(handler)  {   
    	dataHandler = handler;
        setDataCombobox("company_id",dataHandler); 
    });
}
function getHandlerTo(){ 
    $.get(BaseURL+"api/company/autocomplete/vendor",function(handler)  {   
    	dataHandlerTo = handler;
        setDataCombobox("company_to_id",dataHandlerTo); 
    });
}
// function getAirport(){
//     $.get(BaseURL+"api/airport/autocompleteByCity",function(airport)  {   
//     	// dataAirport = airport;  
//         setCombobox("arrive_from",airport); 
//         setDataCombobox("departed_for",airport); 
//         $('#departed_for1').append(dataAirport);
//     });
// }

// function setCombobox(id,data){ 
//     $.each(data, function(key, value) {    
//          $('.'+id)
//              .append($("<option></option>")
//                          .attr("value", value.id)
//                             .text(value.label)); 
//          dataAirport.push($("<option></option>")
//                          .attr("value", value.id)
//                             .text(value.label)); 
//     });
// }
function setDataCombobox(id,data){ 
    $.each(data, function(key, value) {    
         $('.'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
}
function setDataComboboxId(id,data){ 
    $.each(data, function(key, value) {    
         $('#'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
} 
function toTimeZone(time, zone) {
    var formatDate = 'HH:mm';
    var dataTime =  moment(time, formatDate).tz(zone).format(formatDate);
    return dataTime;
}

function toDateTimeZone(date, zone) {
    var formatDate = 'YYYY-MM-DD HH:mm';
    console.log(zone);
    var dataTime =  moment(date, formatDate).tz(zone).format(formatDate);
    console.log(dataTime);
    return dataTime;
}

function deleteFlightLeg(id,elem,counter){
	var r = confirm("Are you sure you want to delete data");
	if (r == true) {
	   $.post(BaseURL + "api/flight_leg/delete/"+id)
	    .done(function (output) { 
	        console.log(output); 
	        removeRow(elem,counter);
	        alert("Delete data successfully"); 
	    })
	    .fail(function (output) { 
	        alert("failed to delete data");
	    });
	}  
    
}

function deleteFlightLegCrew(id,elem,counter){
	var r = confirm("Are you sure you want to delete data");
	if (r == true) {
	   $.post(BaseURL + "api/flight_leg_crew/delete/"+id)
	    .done(function (output) { 
	        console.log(output); 
	        removeRow(elem,counterCrew);
	        alert("Delete data successfully"); 
	    })
	    .fail(function (output) { 
	        alert("failed to delete data");
	    });
	}  
    
}

function deleteFlightLegPassenger(id,elem,counter){
	var r = confirm("Are you sure you want to delete data");
	if (r == true) {
	   $.post(BaseURL + "api/flight_leg_passenger/delete/"+id)
	    .done(function (output) { 
	        console.log(output); 
	        removeRow(elem,counterPassenger);
	        alert("Delete data successfully"); 
	    })
	    .fail(function (output) { 
	        alert("failed to delete data");
	    });
	}  
    
}

function addDataCombobox(idSelect,dataText,dataValue) { 
 	$('.'+idSelect).each(function(i, obj) {
		var option = document.createElement("option");
		  option.text = dataText;
		  option.value = dataValue;
		  $(this).append(option);  
	}); 
}

function saveCrewPassport(id){
    var data = {
        crew_id : id, 
        passport_number : $('#passport_number_crew').val(),
        passport_expiry : $('#passport_expiry_crew').val(), 
        nationality : $('#nationality_crew option:selected').val(),
    }; 
    $.post(BaseURL + "api/crew_passports", data)
    .done(function (output) { 
        console.log(output);
        // count++; 
        addDataCombobox(
            "crew_id",
            $('#family_name_crew').val(),
            id
        ); 
        alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    }); 
}

function saveCrew(){
    var data = {
        codename : $('#codename_crew').val(),
        family_name : $('#family_name_crew').val(),
        surcharge_name : $('#surcharge_name_crew').val(),
        sex : $('#sex_crew option:selected').val(),
        passport_number : $('#passport_number_crew').val(),
        passport_expiry : $('#passport_expiry_crew').val(),
        pob : $('#pob_crew option:selected').val(),
        dob : $('#dob_crew').val(),
        nationality : $('#nationality_crew option:selected').val(),
    }; 
    $.post(BaseURL + "api/crews", data)
    .done(function (output) {
    	if(!isBlank($('#passport_number_crew').val()) && !isBlank($('#passport_expiry_crew').val())){
        	 saveCrewPassport(output.data.crew_id) ; 
    	}else{
    		addDataCombobox(
	            "crew_id",
	            $('#family_name_crew').val(),
	            output.data.crew_id
	        ); 
        	alert("Save data successfully"); 
    	}
        
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

function savePassengerPassport(id){
    var data = {
        passenger_id : id, 
        passport_number : $('#passport_number_passenger').val(),
        passport_expiry : $('#passport_expiry_passenger').val(), 
        nationality : $('#nationality_passenger option:selected').val(),
    }; 
    $.post(BaseURL + "api/passenger_passports", data)
    .done(function (output) { 
        console.log(output);
        // count1++;
        addDataCombobox(
            "passenger_id",
            $('#family_name_passenger').val(),
            id
        );  
        alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

function savePassenger(){
    var data = {
        codename : $('#codename_passenger').val(),
        family_name : $('#family_name_passenger').val(),
        surcharge_name : $('#surcharge_name_passenger').val(),
        codename : $('#codename_passenger').val(),
        sex : $('#sex_passenger option:selected').val(),
        passport_number : $('#passport_number_passenger').val(),
        passport_expiry : $('#passport_expiry_passenger').val(),
        pob : $('#pob_passenger option:selected').val(),
        dob : $('#dob_passenger').val(),
        nationality : $('#nationality_passenger option:selected').val(),
    }; 
    $.post(BaseURL + "api/passengers", data)
    .done(function (output) {
    	if(!isBlank($('#passport_number_passenger').val()) && !isBlank($('#passport_expiry_passenger').val())){
        	savePassengerPassport(output.data.passenger_id);  
    	}else{ 
    		addDataCombobox(
	            "passenger_id",
	            $('#family_name_passenger').val(),
	            output.data.passenger_id
	        );  
        	alert("Save data successfully"); 
    	}
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

$(document).ready(function() {
	// getAirport();
	// $('.arrive_from').select2({
 //        placeholder: 'Select a from'
 //    }); 
	// $('.departed_for').select2({
 //        placeholder: 'Select a from'
 //    }); 
    getHandler();
	$('.company_id').select2({
        placeholder: 'Select a handler'
    }); 
    getHandlerTo();
	$('.company_to_id').select2({
        placeholder: 'Select a handler'
    }); 
	 getCrew();
	$('.crew_id').select2({
        placeholder: 'Select a crew'
    }); 
	getPassenger(); 
	$('.passenger_id').select2({
        placeholder: 'Select a passenger'
    }); 


    $('#nationality_crew').select2({
        placeholder: 'Select a nationality'
    });
    $('#pob_crew').select2({
        placeholder: 'Select a pob'
    });
    $('#nationality_passenger').select2({
        placeholder: 'Select a nationality'
    });
    $('#pob_passenger').select2({
        placeholder: 'Select a pob'
    });

    $('#country_id').select2({
        placeholder: 'Select a country'
    });
    $('#timezone_id').select2({
        placeholder: 'Select a timezone'
    });
    $('#city_id').select2({
        placeholder: 'Select a city'
    });

	$(".route").focus(function() {
        addRoute(counterRoute);
        autoArrive();
        autoDepart();
        getHandler();
        getHandlerTo(); 
        changeFormateDate();
        setDataComboboxId("company_id"+ counterRoute,dataHandler);  
  //       $('#arrive_from'+ counterRoute).append(dataAirport);
  //       $('#departed_for'+ counterRoute).append(dataAirport); 
		// $('.departed_for').select2({
	 //        placeholder: 'Select a from'
	 //    }); 
  //       $('.arrive_from').select2({
	 //        placeholder: 'Select a from'
	 //    }); 
		$('.company_id').select2({
	        placeholder: 'Select a handler'
	    }); 
		$('.company_to_id').select2({
	        placeholder: 'Select a handler'
	    }); 
        $('#countRoute').val(counterRoute);
        $(".departed_for_name_1:last").focus();
        console.log(countRoute);
        counterRoute++;
		changeNumber("numberLeg");
    });

    $(".crew").focus(function() {
        addCrew(counterCrew);
        getCrew();
        setDataComboboxId("crew_id"+counterCrew,dataCrew); 
		$('.crew_id').select2({
	        placeholder: 'Select a crew'
	    }); 
        $('#countCrew').val(counterCrew);
        $(".crew_1:last").focus();
        console.log(counterCrew);
        counterCrew++;
		changeNumber("numberCrew");
    });

    $(".passenger").focus(function() {
        addPassenger(counterPassenger);
        getPassenger();
        setDataComboboxId("passenger_id"+counterPassenger,dataPassenger); 
		$('.passenger_id').select2({
	        placeholder: 'Select a passenger'
	    }); 
        $('#countPassenger').val(counterPassenger);
        $(".passenger_1:last").focus();
        console.log(counterPassenger);
        counterPassenger++;
		changeNumber("numberPassenger");
    });
 
});