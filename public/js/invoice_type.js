$(document).ready(function() {
    $('#invoiceTypes-table').DataTable({
    	scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"invoiceType/getJson",
        columns: [
            {data: 'name', name: 'name'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );