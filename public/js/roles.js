$(document).ready(function() {
    $('#roles-table').DataTable({
        scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"roles/getJson",
        columns: [
            {data: 'roles_name', name: 'roles_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );