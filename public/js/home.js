document.addEventListener("DOMContentLoaded", function() {
    var calendarEl = document.getElementById("calendar");
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: "dayGridMonth",
        themeSystem: "bootstrap",
        // timeZone: 'local',
        locale: 'en',
        // scrollTime: moment().format("HH:mm:ss"),

        // height: 'auto',
        // plugins: [ 'dayGrid','timeGrid', 'list'],
        headerToolbar: {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
        },
        buttonText: {
            dayGridMonth: "Month",
            timeGridWeek: "Week",
            timeGridDay: "Day",
            listWeek: "List Week",
            today: "Today"
        },
        editable: false,
        slotLabelFormat: {
            hour: "numeric",
            minute: "2-digit",
            omitZeroMinute: false,
            hour12: false
        },
        eventTimeFormat: {
            hour: "2-digit",
            minute: "2-digit",
            hour12: false
        },
        //  navLinks: true,
        events: BaseURL + "api/calendar/home",

        eventClick: function(event, element) {
            // Display the modal and set the values to the event values.

            var dateNow = new Date();
            var data = event.event.title;
            var hasil = data.split(" - ");
            var tim = hasil[1].split("-");

            var dtim = tim[0] == IcaoHalim ? "ETD" : "ETA";
            // var dtim2 = tim[0] == "WIIH" ? "LT" : "ZT";
            var route =
                formatDate(event.event.start) +
                " " +
                hasil[0] +
                " " +
                dtim +
                " " +
                formatTime(event.event.start) +
                "LT" +
                " " +
                tim[0] +
                " <img src='img/plane.png' style='width: 20px;'> </img> " +
                tim[1];

            $("#ac").html(
                "<img src='img/plane-up.png' style='width: 20px;'> </img>" +
                    hasil[0]
            );
            $("#route").html(route);
            var ahan = document.getElementById("btnAHAN"); //or grab it by tagname etc
            var gendec = document.getElementById("btnGENDEC"); //or grab it by tagname etc
            $.get(BaseURL + "api/cekAhan/" + event.event.id).done(function(
                output
            ) {
                ahan.href = "/flightCycles/" + event.event.id;
                // gendec.href = "/gendec/"+event.event.id;
            });
            gendec.style.display = "none";
            $.get(
                BaseURL +
                    "api/cekGendec/" +
                    event.event.id +
                    "/" +
                    formatDateToDMY(event.event.start)
            ).done(function(output) {
                if (output.data == "Show") {
                    gendec.style.display = "block";
                    gendec.href = "/gendec/" + event.event.id;
                }
                if (output.data == "Edit") {
                    gendec.style.display = "block";
                    gendec.href = "/gendec/edit/" + event.event.id;
                }
                if (output.data == "NoAction") {
                    gendec.style.display = "none";
                }
            });
            if (event.event.start > dateNow) {
                ahan.href = "/flightCycles/" + event.event.id + "/edit";
            } else {
                ahan.href = "/flightCycles/" + event.event.id;
            }

            $(".modal").modal("show");
        },
        eventDidMount: function(info) {
            if (info.event.extendedProps.background) {
                info.el.style.background = info.event.extendedProps.background;
            }

            // info.el.innerHTML = info.el.innerHTML.replace('$ICON', "<em class='far fa-"+info.event.extendedProps.icon+"'></em>");
            // if
            // element.find('.fc-event-title').append("<br/>" + event.description);
            // console.log("print", info.el.innerHTML)
        },
        // eventContent: function(args, createElement) {
        //     const icon = args.event._def.extendedProps.icon;
        //     const text =
        //         "<img src='img/plane-up.png' style='width: 20px;'> </img>" +
        //         args.event._def.title;
        //     return {
        //         html: text
        //     };
        // }
    });
    calendar.render();
});

function formatDate(date) {
    var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month].join("/");
}

function formatTime(time) {
    var d = new Date(time),
        h = (d.getHours() < 10 ? "0" : "") + d.getHours(),
        m = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
    return [h, m].join(":");
}
