var countPassport = 0;
var countVisa = 0;
var dataCountry = [];
var dataVisa = [];
function format ( d ) {
  // `d` is the original data object for the row

  if(d){
      // console.log("d",d.passports);
      var html = "<div class='row'><div class='col-6'>"+
      "<p class='text-center'><strong>Passport</strong></p>"+
      "<table><thead>"+
          "<tr>"+
              "<th>No</th>"+
              "<th>Passport Number</th>"+
              "<th>Passport Expired</th>"+
              "<th>Nationality</th>"+
              // "<th>Action</th>"+
          "</tr>"+
      "</thead>"
      var no = 1, no2 = 1;
      for (var i = 0; i < d.crew_passports.length; i++) {
       let url = "crewPassports/:id".replace(':id', d.crew_passports[i].crew_passport_id);
       html += "<tr><td>" + no + "</td>"+
       "<td>" + d.crew_passports[i].passport_number + "</td>"+
       "<td>" + formatDateToDMY(d.crew_passports[i].passport_expiry) + "</td>"+
       "<td>" + d.crew_passports[i].nationality.name + "</td>"
       // "<td>"+
       //    "<div class='btn-group'>"+
       //    "<a href='"+url+"' class='btn btn-ghost-success'><i class='fa fa-eye'></i></a>"+
       //    "</div>"+
       // "</td>"

      ;
       no++
      }

    html += "</tr></table></div>"+

    "<div class='col-6'>"+
    "<p class='text-center'><strong>Visa</strong></p>"+
    "<table><thead>"+
        "<tr>"+
            "<th>No</th>"+
            "<th>Visa Number</th>"+
            "<th>Active Since</th>"+
            "<th>Active Until</th>"+
            "<th>Nationality</th>"+
            "<th>Visa Type</th>"+
        "</tr>"+
    "</thead>"
    for (var j = 0; j < d.crew_visas.length; j++) {
       html += "<tr><td>" + no2 + "</td>"+
       "<td>" + d.crew_visas[j].visa_number + "</td>"+
       "<td>" + formatDateToDMY(d.crew_visas[j].active_since) + "</td>"+
       "<td>" + formatDateToDMY(d.crew_visas[j].active_until) + "</td>"+
       "<td>" + d.crew_visas[j].nationality.name + "</td>"+
       "<td>" + d.crew_visas[j].visa_types.name + "</td>"
        // "<td>"+
        //    "<div class='btn-group'>"+
        //    "<a href='crewVisas/"+d.crew_visas[j].crew_visa_id+"' class='btn btn-ghost-success'><i class='fa fa-eye'></i></a>"+
        //    "</div>"+
        // "</td>"
        ;
       no2++
    }
    "</div>"+
    "</div>";

        return html;
    }
    return ""

}

$(document).ready(function() {
 $('#crews-table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'csvHtml5',
            title: window.location.pathname
        },
        {
            extend: 'excelHtml5',
            title: window.location.pathname
        },
        {
            extend: 'pdfHtml5',
            title: window.location.pathname
        }
     
    ],
  destroy: true,
  processing: true,
  serverSide: true,
  ajax: BaseURL + "crews/getJson",
  columns: [
    //   {
    //       data: 'codename',
    //       name: 'crew.codename'
    //   },
      {
          data: 'family_name',
          name: 'crew.family_name'
      },
      {
        data: 'surcharge_name',
        name: 'crew.surcharge_name'
    },
      {
        data: 'sex',
        name: 'crew.sex'
      },
      {
        data: 'name',
        name: 'country.name'
      },
      {
          data: 'passport_number',
          name: 'crew.passport_number'
      },
      {
        data: 'passport_expiry',
        "render": function (data) {
          // console.log("data",data);
          if (!isBlank(data)) {
            var d = new Date(data),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
          var str = month + "/" + day + "/" + year;
          return str;  
          }else{
            return "";
          }
        },
        name: 'crew.passport_expiry'
    },
    
      {
          data: 'pob_name',
          name: 'ct.name'
      },
      {
          data: 'dob',
          "render": function (data) {
              // console.log("data",data);
             if (!isBlank(data)) {
                var d = new Date(data),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
          var str = month + "/" + day + "/" + year;
              return str;
             }else{
                 return "";
             }
          },
          name: 'crew.dob'
      },
      {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false
      },
  ]
  } );

  addRowPassport();
  addRowVisa();
} );

function removeRow(elem){
	$(elem).closest("tr").remove();
}

function addRowPassport(){
    $('#addRowPassport').click(function(){
      console.log("click");
        countPassport++;
        $('#rowPassport').append("<tr>"+
        "<td><input type='text' name='passport_number[]' class='form-control'></td>"+
        "<td><input type='date' name='passport_expiry[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy'  class='tm form-control'></td>"+
        // "<td><input type='text' target='country_id_passport-"+countPassport+"' id='nationality_passport-"+countPassport+"' onclick=applyAutoComplete('nationality_passport-"+ countPassport +"','api/countries/autocomplete','') class='form-control'>"+
        //   "<input type='hidden' id='country_id_passport-"+countPassport+"' name='nationality_passport[]'></td>"+
         "<td>"+
            "<select id='country_id_passport-" + countPassport + "' name='nationality_passport[]' class='nationality_passport form-control'>"+
            "<option value='' readonly>Select a nationality</option></select>"+
            "</td>"+
        "<td><a href='#' id='#removeRow' class='btn btn-danger' onclick='removeRow(this)'>Delete</a></td></tr>");     
        changeFormateDate();
        setDataComboboxId("country_id_passport-" + countPassport,dataCountry);
        $('.nationality_passport').select2({
            placeholder: 'Select a nationality'
        });

    });
}

function addRowVisa(){
    $('#addRowVisa').click(function(){
        countVisa++;
        console.log("click yr",countVisa);
        $('#rowVisa').append("<tr>"+
        "<td><input type='text' name='visa_number[]' class='form-control'></td>"+
        // "<td><input type='text' id='visa_type_name-"+countVisa+"' target='visa_type_id-"+countVisa+"' onclick=applyAutoComplete('visa_type_name-"+ countVisa +"','api/visaTypes/autocomplete','')  class='form-control'>"+
        // "<input type='hidden' id='visa_type_id-"+countVisa+"' name='visa_type_id[]'></td>"+
        // "<td><input type='text' target='country_id_visa-"+countVisa+"' id='nationality_visa-"+countVisa+"' onclick=applyAutoComplete('nationality_visa-"+ countVisa +"','api/countries/autocomplete','') class='form-control'>"+
        //   "<input type='hidden' id='country_id_visa-"+countVisa+"' name='country_id[]'></td>"+
        "<td>"+
            "<select id='visa_type_id-" + countVisa + "' name='visa_type_id[]' class='visa_type_id form-control'>"+
            "<option value='' readonly>Select a visa type</option></select>"+
            "</td>"+
        "<td>"+
            "<select id='nationality_visa-" + countVisa + "' name='country_id[]' class='nationality_visa form-control'>"+
            "<option value='' readonly>Select a nationality</option></select>"+
            "</td>"+
          "<td><input type='date' name='active_since[]'  data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy'  class='tm form-control'></td>"+
          "<td><input type='date' name='active_until[]'  data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy'  class='tm form-control'></td>"+
        "<td><a href='#' id='#removeRow' class='btn btn-danger' onclick='removeRow(this)'>Delete</a></td></tr>");

        changeFormateDate();
        setDataComboboxId("nationality_visa-" + countVisa ,dataCountry);
        setDataComboboxId("visa_type_id-" + countVisa ,dataVisa);
        $('.nationality_visa').select2({
            placeholder: 'Select a nationality'
        });
        $('.visa_type_id').select2({
            placeholder: 'Select a visa type'
        });
    });
}


function getCountry(){
    $.get(BaseURL+"api/countries/autocomplete",function(country)  {  
       dataCountry = country; 
        setDataCombobox("nationality_passport",country); 
        setDataCombobox("nationality_visa",country);
    });
}

function getVisaType(){
    $.get(BaseURL+"api/visaTypes/autocomplete",function(visa)  {  
       dataVisa = visa; 
        setDataCombobox("visa_type_id",visa);  
    });
}

function setDataCombobox(id,data){
    $.each(data, function(key, value) {    
         $('.'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
}
function setDataComboboxId(id,data){
    $.each(data, function(key, value) {    
         $('#'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
}



function deleteCrewPassport(id){
  var x = confirm("Are you sure you want to delete?");
  if (x){  
        $.post(BaseURL + "api/crew_passport/delete/"+id)
        .done(function (outputAircraftType) {  
            document.getElementById(id).remove();
            alert("delete data successfully"); 
        })
        .fail(function (outputAircraftType) {  
            alert("failed to delete data");
        });
  }else{
    return false;
  } 
}

function deleteCrewVisa(id){
  var x = confirm("Are you sure you want to delete?");
  if (x){  
        $.post(BaseURL + "api/crew_visa/delete/"+id)
        .done(function (outputAircraftType) {  
            document.getElementById(id).remove();
            alert("delete data successfully"); 
        })
        .fail(function (outputAircraftType) {  
            alert("failed to delete data");
        });
  }else{
    return false;
  } 
}

$(document).ready(function() {
    getCountry();
    getVisaType(); 
    $('#nationality').select2({
        placeholder: 'Select a nationality'
    });
    $('.nationality_passport').select2({
        placeholder: 'Select a nationality'
    });
    $('.nationality_visa').select2({
        placeholder: 'Select a nationality'
    });
    $('.visa_type_id').select2({
            placeholder: 'Select a visa type'
    });
    $('#pob').select2({
        placeholder: 'Select a pob'
    });
    
} );
