var countPassport = 0;
var dataCountry = [];
function format(d) {
    // `d` is the original data object for the row

    if (d) {
        // console.log("d",d.passports);
        var html = "<div class='row'><div class='col-6'>" +
            "<p class='text-center'><strong>Passport</strong></p>" +
            "<table><thead>" +
            "<tr>" +
            "<th>No</th>" +
            "<th>Passport Number</th>" +
            "<th>Passport Expired</th>" +
            "<th>Nationality</th>" +
            "<th>Action</th>" +
            "</tr>" +
            "</thead>"
        var no = 1,
            no2 = 1;
        for (var i = 0; i < d.passports.length; i++) {
            let url = "passengerPassports/:id".replace(':id', d.passports[i].passenger_passport_id);
            html += "<tr><td>" + no + "</td>" +
                "<td>" + d.passports[i].passport_number + "</td>" +
                "<td>" + formatDateToDMY(d.passports[i].passport_expiry) + "</td>" +
                "<td>" + d.passports[i].nationality.name + "</td>" +
                "<td>" +
                "<div class='btn-group'>" +
                "<a href='" + url + "' class='btn btn-ghost-success'><i class='fa fa-eye'></i></a>" +
                "</div>" +
                "</td>"

            ;
            no++
        }

        // html += "</tr></table></div>"+
        //
        // "<div class='col-6'>"+
        // "<p class='text-center'><strong>Visa</strong></p>"+
        // "<table><thead>"+
        //     "<tr>"+
        //         "<th>No</th>"+
        //         "<th>Visa Number</th>"+
        //         "<th>Active Since</th>"+
        //         "<th>Active Until</th>"+
        //     "</tr>"+
        // "</thead>"
        // for (var j = 0; j < d.passanger_visa.length; j++) {
        //    html += "<tr><td>" + no2 + "</td>"+
        //    "<td>" + d.passanger_visa[j].visa_number + "</td>"+
        //    "<td>" + formatDateToDMY(d.passanger_visa[j].active_since) + "</td>"+
        //     "<td>" + formatDateToDMY(d.passanger_visa[j].active_until) + "</td>";
        //    no2++
        // }
        // "</div>"+
        // "</div>";

        return html;
    }
    return ""

}

$(document).ready(function () {
    $('#passenger-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: window.location.pathname
            },
            {
                extend: 'excelHtml5',
                title: window.location.pathname
            },
            {
                extend: 'pdfHtml5',
                title: window.location.pathname
            }
         
        ],
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: BaseURL + "passengers/getJson",
        columns: [
            {
                data: 'family_name',
                name: 'passenger.family_name'
            },
            {
                data: 'surcharge_name',
                name: 'passenger.surcharge_name'
            },
            {
              data: 'sex',
              name: 'passenger.sex'
            },
            {
                data: 'name',
                name: 'country.name'
              },
            {
                data: 'passport_number',
                name: 'passenger.passport_number'
            },
            {
              data: 'passport_expiry',
              "render": function (data) {
                // console.log("data",data);
                // var myDate = new Date(data);
                // var month = [
                //     "Jan",
                //     "Feb",
                //     "Mar",
                //     "Apr",
                //     "May",
                //     "Jun",
                //     "Jul",
                //     "Aug",
                //     "Sep",
                //     "Oct",
                //     "Nov",
                //     "Dec",
                // ][myDate.getMonth()];
                // var str = myDate.getDate() + "/" + month + "/" + myDate.getFullYear();
                    if (!isBlank(data)) {
                        var d = new Date(data),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    var str = month + "/" + day + "/" + year;
                        return str;
                    } else{
                        return "";
                    }
                },
                name: 'passenger.passport_expiry'
             },
          
            {
                data: 'ci_name',
                name: 'city.name'
            },
            {
                data: 'dob',
                "render": function (data) {
                    // console.log("data",data);
                    // var myDate = new Date(data);
                    // var month = [
                    //     "Jan",
                    //     "Feb",
                    //     "Mar",
                    //     "Apr",
                    //     "May",
                    //     "Jun",
                    //     "Jul",
                    //     "Aug",
                    //     "Sep",
                    //     "Oct",
                    //     "Nov",
                    //     "Dec",
                    // ][myDate.getMonth()];
                    // var str = myDate.getDate() + "/" + month + "/" + myDate.getFullYear();
                    if (!isBlank(data)) {
                        var d = new Date(data),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                  var str = month + "/" + day + "/" + year;
                    return str;
                    } else{
                        return "";
                    }
                    
                },
                name: 'passenger.dob'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });

    
    addRowPassport();
});


function deletePassport(id){
  var x = confirm("Are you sure you want to delete?");
  if (x){  
        $.post(BaseURL + "api/passenger_passport/delete/"+id)
        .done(function (outputAircraftType) {  
            document.getElementById(id).remove();
            alert("delete data successfully"); 
        })
        .fail(function (outputAircraftType) {  
            alert("failed to delete data");
        });
  }else{
    return false;
  } 
}

function removeRow(elem) {
    $(elem).closest("tr").remove();
}

function addRowPassport() {
    $('#addRowPassport').click(function () {
        console.log("click");
        countPassport++;
        $('#rowPassport').append("<tr>" +
            "<td><input type='text' name='passport_number[]' class='form-control'></td>" +
            "<td><input type='date' name='passport_expiry[]' data-date-format='MM/DD/YYYY' placeholder='mm/dd/yyyy' class='tm form-control'></td>" +
            // "<td><input type='text' target='country_id_passport-" + countPassport + "' id='nationality_passport-" + countPassport + "' onclick=applyAutoComplete('nationality_passport-" + countPassport + "','api/countries/autocomplete','') class='form-control'>" +
            // "<input type='hidden' id='country_id_passport-" + countPassport + "' name='nationality_passport[]'></td>" +
            "<td>"+
            "<select id='nationality_passport-" + countPassport + "' name='nationality_passport[]' class='nationality_passport form-control'>"+
            "<option value='' readonly>Select a nationality</option></select>"+
            "</td>"+
            "<td><a href='#' id='#removeRow' class='btn btn-danger' onclick='removeRow(this)'>Delete</a></td></tr>");
        changeFormateDate();
        setDataComboboxId("nationality_passport-" + countPassport,dataCountry);
        $('.nationality_passport').select2({
            placeholder: 'Select a nationality'
        });
    });
}

function getCountry(){
    $.get(BaseURL+"api/countries/autocomplete",function(country)  {  
       dataCountry = country; 
        setDataCombobox("nationality_passport",country); 
    });
}

function setDataCombobox(id,data){
    $.each(data, function(key, value) {    
         $('.'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
}

function setDataComboboxId(id,data){ 
        console.log(id);
    $.each(data, function(key, value) {  
         $('#'+id)
             .append($("<option></option>")
                         .attr("value", value.id)
                            .text(value.label)); 
    });
}


$(document).ready(function() {
    getCountry();
    $('#nationality').select2({
        placeholder: 'Select a nationality'
    });
    $('.nationality_passport').select2({
        placeholder: 'Select a nationality'
    });
    $('#pob').select2({
        placeholder: 'Select a pob'
    });
    
} );