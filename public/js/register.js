function getListAutoCompleteRoles(displayID,hiddenID,endPoint) { // Get List Api for Autocomlete
    console.log("id", displayID, hiddenID, endPoint);
    $("#" + displayID).autocomplete({
        source: function(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            $.ajax({
                url: BaseURL+"api/"+endPoint,
                contentType: 'application/json',
                dataType: 'json',
                data: {
                    q: request.term
                },
                success: function(data) {
                    // console.log(data);

                    response($.map(data.data, function(v, i) {
                      console.log(v,"v");
                        var text = v.roles_name;
                        if (text && (!request.term || matcher.test(text))) {
                            return {

                                id: v.roles_id,
                                label: v.roles_name

                            };
                        }
                    }));
                }
            });
        },
        //minLength: 1,
        select: function(event, ui) {
            $('#'+hiddenID).val(ui.item.id);
            // console.log(ui.item.id);
        },
        change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $('#empty-message').show();
            } else {
                $('#empty-message').hide();
            }
        },

    });
}

$(document).ready(function() {

});
