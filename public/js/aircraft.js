  $(document).ready(function() {
    $('#aircraft-table').DataTable({
        scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"aircraft/getJson",
        columns: [
            {data: 'aircraft_registration', name: 'aircraft_registration'},
            {data: 'aircraft_name', name: 'aircraft_type.aircraft_name'},
            {data: 'mtow', name: 'aircraft_type.mtow'},
            {data: 'fuel_capacity', name: 'fuel_capacity'}, 
            {data: 'total_number_of_seat', name: 'total_number_of_seat'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  } );

function cekDataAircraftType(data){
  console.log(data);
    var elems = document.getElementsByClassName('aircraftType');
  if(data == null || data == ""){
    for (var i=0;i<elems.length;i+=1){
      elems[i].style.display = 'none';
    } 
  }else{
    for (var i=0;i<elems.length;i+=1){
      elems[i].style.display = 'grid';
    } 
    getDataAircraftType(data);
  }
}

function getDataAircraftType(id){
    $.get(BaseURL+"api/aircraft_types/"+id,function(aircraftType)  {
        $('#iata_code_add').val(aircraftType.data.iata_code);
        $('#aircraft_brand_add').val(aircraftType.data.aircraft_brand);
        $("#aircraft_name_add").val(aircraftType.data.aircraft_name); 
        $('#designator_add').val(aircraftType.data.designator);
        $("#mtow_add").val(aircraftType.data.mtow); 
    });
}

function saveAircraftType(){
    var dataAircraftType = {
        iata_code : $('#iata_code').val(),
        aircraft_brand : $('#aircraft_brand').val(),
        aircraft_name : $('#aircraft_name').val(),
        designator : $('#designator').val(),
        mtow : $('#mtow').val(), 
    }; 
    $.post(BaseURL + "api/aircraft_types", dataAircraftType)
    .done(function (outputAircraftType) { 
        console.log(outputAircraftType);
        addDataCombobox(
            "aircraft_type",
            $('#aircraft_name').val(),
            outputAircraftType.data.id_aircraft_type
        );
        document.getElementById('aircraftType').style.display='block';
        getDataAircraftType(outputAircraftType.data.id_aircraft_type);
        alert("Save data successfully"); 
    })
    .fail(function (outputAircraftType) { 
        document.getElementById('aircraftType').style.display='none';
        alert("failed to save data");
    });
}

function deleteFile(file,id){
  var x = confirm("Are you sure you want to delete?");
  if (x){
      if(file == "file1"){
          dataAircraftType = {
              certificate_of_registration_file : $('#certificate_of_registration_file').val(), 
          }; 
        }
        if(file == "file2"){
          dataAircraftType = {
              certificate_of_insurance_file : $('#certificate_of_insurance_file').val(), 
          }; 
        }
        if(file == "file3"){
          dataAircraftType = {
              certificate_of_airworthiness_file : $('#certificate_of_airworthiness_file').val(), 
          }; 
        }
        
        $.post(BaseURL + "api/aircrafts/update/"+id, dataAircraftType)
        .done(function (outputAircraftType) {  
            document.getElementById(file).remove();
            alert("delete data successfully"); 
        })
        .fail(function (outputAircraftType) {  
            alert("failed to delete data");
        });
  }else{
    return false;
  } 
}

function validasiEkstensi(id){
    var inputFile = document.getElementById(id);
    var pathFile = inputFile.value;
    var ekstensiOk = /(\.jpg|\.png|\.pdf)$/i;
    if(!ekstensiOk.exec(pathFile)){
        alert('Please upload the file with the .jpg /.png /.pdf extension');
        inputFile.value = '';
        return false;
    }
}

function addDataCombobox(idSelect,dataText,dataValue) {
  var x = document.getElementById(idSelect);
  var option = document.createElement("option");
  option.text = dataText;
  option.value = dataValue;
  option.selected = "selected";
  x.add(option); 
}

$( document ).ready(function() {
    $('#country').select2({
        placeholder: 'Select a country'
    });  
});
