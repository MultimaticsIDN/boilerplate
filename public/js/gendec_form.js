var count = $('#count').val();
var count1 = $('#count1').val(); 
var numItems = $('.jmlPassenger').length 
var departCountry = $('#departCountry').val(); 
var arriveCountry = $('#arriveCountry').val(); 

function getDatacrew(id){
    count++;  
    $.post(BaseURL + "api/flight_leg_crews",{
        flight_leg_id: $("#flight_leg_id").val(),
        crew_id:id
    })
    .done(function (output) {
         $.get(BaseURL+"api/crews/"+id,function(crew)  {
            var surcharge = "";
            if(!isBlank(crew.data.surcharge_name)){
                surcharge = crew.data.surcharge_name;
            }
            var country = "";
            if(!isBlank(crew.data.country.iso3)){
                country = crew.data.country.iso3;
            }
            var btn = "<a href='#' id='#remove' class='btn btn-danger'  onclick='deleteCrew(this)'><i class='fa fa-trash'></i></a>";
            $("#crew_id_" + count).val(crew.data.crew_id);
            $("#crew_name_" + count).html(surcharge +" " + crew.data.family_name); 
            $("#sex_" + count).html(crew.data.sex);
            if(departCountry != 360 || arriveCountry  != 360){
                $("#passport_" + count).html(crew.data.passport_number);
                $("#expiry_" + count).html(formatDateToMDY(crew.data.passport_expiry)); 
                $("#dob_" + count).html(formatDateToMDY(crew.data.dob)); 
            }
            $("#nat_" + count).html(country);
            $("#btn_" + count).html(btn);
            $("#act_" + count).show();
            alert("Data added successfully");
        });
    })
    .fail(function(data) { 
        alert("Failed to Save Data");
    });
	
}
 
function getDataPassenger(id){
    count1++;
    $.post(BaseURL + "api/flight_leg_passengers",{
        flight_leg_id: $("#flight_leg_id").val(),
        passenger_id:id
    })
    .done(function (output) {
         $.get(BaseURL+"api/passengers/"+id,function(passenger)  {

            var surcharge = "";
            if(!isBlank(passenger.data.surcharge_name)){
                surcharge = passenger.data.surcharge_name;
            }
            
            var country = "";
            if(!isBlank(passenger.data.country.iso3)){
                country = passenger.data.country.iso3;
            }
            if(numItems > 15){
                numItems = $('.jmlPassenger').length;
                var passport = passenger.data.passport_number;
                var dob = formatDateToMDY(passenger.data.dob);
                var expiry = formatDateToMDY(passenger.data.passport_expiry);
                if(departCountry == 360 && arriveCountry  == 360){
                    passport = "";
                    dob = "";
                    expiry = ""; 
                }
                var row = "";
                  row += "<tr>"; 
                  row += "<td class='jmlPassenger'>"+count1+"</td>";
                  row += "<td style='display:none;'><input name='passenger_id[]' type='hidden' id='passenger_id_"+count1+"' value='"+passenger.data.passenger_id+"'></td>";
                  row += "<td id='passenger_name_"+count1+"'> "+surcharge +" "+ passenger.data.family_name+"</td> ";
                  row += "<td id='passenger_sex_"+count1+"'>"+passenger.data.sex+"</td>";
                  row += "<td id='passenger_passport_"+count1+"'>"+passport+"</td>";
                  row += "<td id='passenger_dob_"+count1+"'>"+dob+"</td>";
                  row += "<td id='passenger_expiry_"+count1+"'>"+expiry+"</td>";
                  row += "<td id='passenger_nat_"+count1+"'>"+country+"</td>";
                  row += "<td id='passenger_btn_"+count1+"'>";
                   
                       row += "<div class='btn-group' id='passenger_act_"+count1+"'>";
                        row += "<a href='#' id='#remove' class='btn btn-danger'  onclick='deletePassenger(this)'><i class='fa fa-trash'></i></a>";
                      row += "</div>"; 
                     
                  row += "</td>";
                row += "</tr>";
                console.log("MASUK1");
                $('#dataPassenger').append(row);
            }else{
                var btn = "<a href='#' id='#remove' class='btn btn-danger'  onclick='deletePassenger(this)'><i class='fa fa-trash'></i></a>";
                $("#passenger_id_" + count1).val(passenger.data.passenger_id);
                $("#passenger_name_" + count1).html(surcharge +" "+ passenger.data.family_name);
                $("#passenger_sex_" + count1).html(passenger.data.sex); 
                if(departCountry != 360 || arriveCountry  != 360){
                    $("#passenger_passport_" + count1).html(passenger.data.passport_number);
                    $("#passenger_dob_" + count1).html(formatDateToMDY(passenger.data.dob));
                    $("#passenger_expiry_" + count1).html(formatDateToMDY(passenger.data.passport_expiry));
                }
                $("#passenger_nat_" + count1).html(country);
                $("#passenger_btn_" + count1).html(btn);
                $("#passenger_act_" + count1).show();
                console.log("MASUK");
            }
                changeNumber("jmlPassenger");
            
             alert("Data added successfully");
        });
    })
    .fail(function(data) { 
        alert("Failed to Save Data");
    });
	
}

function deletePassenger(data){
    removeRow(data,'passenger_id_'+count1,'api/flight_leg_passengers/destroy/FlightLegIdAndPassangerId');
}

function deleteCrew(data){
    removeRow(data,'crew_id_'+count,'api/flight_leg_crews/destroy/FlightLegIdAndCrewId');
}

function applyAutoCompletePassenger(){
    $( "#passengerID" ).autocomplete({
        source: BaseURL+"api/passenger/autocomplete",
        //minLength: 0,
        selectFirst: true, //here
        select: function( event, ui ) {
            // count1++;
            var target = $(this).attr("target");
            $("#"+target).val(ui.item.id); 
            // getDataPassenger(ui.item.id, count1);
        },
         change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $("#empty-message").show();
            } else {
                $("#empty-message").hide();
            }
        },
    });
}

$('#table tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        row.child( format(row.data()) ).show();
        tr.addClass('shown');
    }
} );


function removeRow(elem,id,url){
    numItems = $('.jmlPassenger').length;
	$.post(BaseURL + url,{
        flight_leg_id: $("#flight_leg_id").val(),
        id:$("#"+id).val()
    })
    .done(function (output) { 
            $(elem).closest("tr").remove();
            changeNumber("jmlPassenger");
             alert("Data deleted successfully"); 
    })
    .fail(function(data) { 
        alert("Failed to Delete Data");
    });
}


function addRow(){
    $('#addRow').click(function(){
        countPassport++;
        $('tbody').append("<tr><td><input type='text' name='passport_number[]' class='form-control'></td><td><input type='date' name='passport_expiry[]' class='form-control'></td><td><input type='text' id='nationality-"+countPassport+"' onclick='getListAutoComplete('nationality-"+countPassport+",country_id-"+countPassport+",countries);' name='nationality[]' class='form-control'><input type='hidden' id='country_id-"+countPassport+"'></td><td><a href='#' id='#removeRow' class='btn btn-danger' onclick='removeRow(this)'>Delete</a></td></tr>");
    });


}

function show(id){
    var crew_id = $('#crew_id_'+id).val();
    // console.log(crew_id );
    window.location.href = '/crews/'+crew_id;
    // window.href = 'crews/'+crew_id;
}

function showPassenger(id){
    var passenger_id = $('#passenger_id_'+id).val();
    console.log(passenger_id );
    window.location.href = '/passengers/'+ passenger_id;
    // window.href = 'crews/'+crew_id;
}

// function applyAutoComplete(){
//     $( "#crewID" ).autocomplete({
//         source: BaseURL+"api/crew/autocomplete",
//         //minLength: 2,
//         selectFirst: true, //here
//         select: function( event, ui ) {
//             count++; 
//             var target = $(this).attr("target");
//             $("#"+target).val(ui.item.id);
//             console.log(ui.item);
//             getDatacrew(ui.item.id, count);
//         },
//          change: function(event, ui) {
//             if (!ui.item) {
//                 $(this).val("");
//                 $("#empty-message").show();
//             } else {
//                 $("#empty-message").hide();
//             }
//         },
//     });
// }

function saveCrewPassport(id){
    var data = {
        crew_id : id, 
        passport_number : $('#passport_number_crew').val(),
        passport_expiry : $('#passport_expiry_crew').val(), 
        nationality : $('#nationality_crew option:selected').val(),
    }; 
    $.post(BaseURL + "api/crew_passports", data)
    .done(function (output) { 
        console.log(output);
        // count++; 
        addDataCombobox(
            "crew",
            $('#family_name_crew').val(),
            id
        );
         getDatacrew(id);
        alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    }); 
}

function saveCrew(){
    var data = {
        codename : $('#codename_crew').val(),
        family_name : $('#family_name_crew').val(),
        surcharge_name : $('#surcharge_name_crew').val(),
        sex : $('#sex_crew option:selected').val(),
        passport_number : $('#passport_number_crew').val(),
        passport_expiry : $('#passport_expiry_crew').val(),
        pob : $('#pob_crew option:selected').val(),
        dob : $('#dob_crew').val(),
        nationality : $('#nationality_crew option:selected').val(),
    }; 
    $.post(BaseURL + "api/crews", data)
    .done(function (output) {
        saveCrewPassport(output.data.crew_id) ;
        // console.log(output);
        // count++; 
        // addDataCombobox(
        //     "crew",
        //     $('#family_name_crew').val(),
        //     output.data.crew_id
        // );
        //  getDatacrew(output.data.crew_id, count);
        // alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}


function savePassengerPassport(id){
    var data = {
        passenger_id : id, 
        passport_number : $('#passport_number_passenger').val(),
        passport_expiry : $('#passport_expiry_passenger').val(), 
        nationality : $('#nationality_passenger option:selected').val(),
    }; 
    $.post(BaseURL + "api/passenger_passports", data)
    .done(function (output) { 
        console.log(output);
        // count1++;
        addDataCombobox(
            "passenger",
            $('#family_name_passenger').val(),
            id
        ); 
        getDataPassenger(id);
        alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

function savePassenger(){
    var data = {
        codename : $('#codename_passenger').val(),
        family_name : $('#family_name_passenger').val(),
        surcharge_name : $('#surcharge_name_passenger').val(),
        codename : $('#codename_passenger').val(),
        sex : $('#sex_passenger option:selected').val(),
        passport_number : $('#passport_number_passenger').val(),
        passport_expiry : $('#passport_expiry_passenger').val(),
        pob : $('#pob_passenger option:selected').val(),
        dob : $('#dob_passenger').val(),
        nationality : $('#nationality_passenger option:selected').val(),
    }; 
    $.post(BaseURL + "api/passengers", data)
    .done(function (output) {
        savePassengerPassport(output.data.passenger_id); 
        // console.log(output);
        // count1++;
        // addDataCombobox(
        //     "passenger",
        //     $('#family_name_passenger').val(),
        //     output.data.passenger_id
        // ); 
        // getDataPassenger(output.data.passenger_id, count1);
        // alert("Save data successfully"); 
    })
    .fail(function (output) { 
        alert("failed to save data");
    });
}

function addDataCombobox(idSelect,dataText,dataValue) {
  var x = document.getElementById(idSelect);
  var option = document.createElement("option");
  option.text = dataText;
  option.value = dataValue;
  option.selected = "selected";
  x.add(option); 
}

function printDiv() {
    window.print();
}

$(document).ready(function(){
    $('#crew').select2({
        placeholder: 'Select a crew'
    });

    $('#passenger').select2({
        placeholder: 'Select a passenger'
    });
    $('#nationality_crew').select2({
        placeholder: 'Select a nationality'
    });
    $('#pob_crew').select2({
        placeholder: 'Select a pob'
    });
    $('#nationality_passenger').select2({
        placeholder: 'Select a nationality'
    });
    $('#pob_passenger').select2({
        placeholder: 'Select a pob'
    });
});
