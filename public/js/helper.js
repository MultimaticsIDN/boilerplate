var BaseURL = "http://localhost:8000/";
// var BaseURL = "https://karisma.support/";

var IcaoHalim = "WIHH";

function replaceKoma(data){ 
    return data.replaceAll(",", "");
}

function formatCurrencyWithoutRp(num) {

    num = num.toString().replace(/\|/g,'');
    if(isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+
        num.substring(num.length-(4*i+3));
   return ((sign)?'':'-') + num + '.00' ;

}
 
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function formatDateToDMY(date){
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('-');

}
function formatDateToYMD(date){
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');

}
function formatDateToMDY(date){
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [month, day, year].join('/');

}

function applyAutoComplete(displayID,endPoint,message){
    $( "#"+displayID ).autocomplete({
        source: BaseURL+endPoint,
        //minLength: 1,
        selectFirst: true, //here
        select: function( event, ui ) {
            var target = $(this).attr("target");
            console.log(target);
            $("#"+target).val(ui.item.id);
        },
         change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $('#'+message).show();
            } else {
                $('#'+message).hide();
            }
        },
    });
}

function changeNumber(elem){
    $('.'+elem).each(function(i, obj) {
        var j = i+1;
        $(this).html("<center>"+ j +"</center>"); 
    });
}

function getListAutoComplete(displayID,hiddenID,endPoint) { // Get List Api for Autocomlete
    console.log(displayID,hiddenID, endPoint,"id", displayID, hiddenID, endPoint);
    $("#" + displayID).autocomplete({
        source: function(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            $.ajax({
                url: BaseURL+"api/"+endPoint,
                contentType: 'application/json',
                dataType: 'json',
                data: {
                    q: request.term
                },
                success: function(data) {
                    // console.log(data.{{data}});

                    response($.map(data.data, function(v, i) {
                        var text = v.name;
                        if (text && (!request.term || matcher.test(text))) {
                            return {

                                id: v.company_id,
                                label: v.name

                            };
                        }
                    }));
                }
            });
        },
        //minLength: 1,
        select: function(event, ui) {
            $('#'+hiddenID).val(ui.item.id);
            // console.log(ui.item.id);
        },
        change: function(event, ui) {
            if (!ui.item) {
                $(this).val("");
                $('#empty-message').show();
            } else {
                $('#empty-message').hide();
            }
        },

    });
}

function formatMoney(number) {
   // return Number((number).toFixed(2)).toLocaleString();
   return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function changeFormateDate(){
     $(".tm").on("change", function() {
     if(this.value){
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
      } else{ 
        $(this).attr('data-date', 'mm/dd/yyyy'); 
      }
    }).trigger("change")
}

$(document).ready(function(){
    $('.separator').mask('#,##0.00', {reverse: true}); 
    $(".floatNumberField").change(function() {
        $(this).val(parseFloat($(this).val()).toFixed(2));
    });
    changeFormateDate();

    $('#formid').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
          e.preventDefault();
          return false;
        }
      });
});


