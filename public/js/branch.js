$(document).ready(function() {
    $('#branches-table').DataTable({
    	scrollX : true,
        processing: true,
        serverSide: true,
        dom: 'Bfrtip', 
        ajax: BaseURL+"branch/getJson",
        columns: [
            {data: 'icao_code', name: 'icao_code'},
            {data: 'name', name: 'name'},
            {data: 'address', name: 'address'}, 
            {data: 'phone', name: 'phone'},  
            {
                data: 'established',
                "render": function (data) { 
                  if (!isBlank(data)) {
                    var d = new Date(data),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;
                  var str = month + "/" + day + "/" + year;
                  return str;  
                  }else{
                    return "";
                  }
                },
                name: 'established'
            },
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        buttons: [
            {
                extend: 'csvHtml5',
                title: window.location.pathname
            },
            {
                extend: 'excelHtml5',
                title: window.location.pathname
            },
            {
                extend: 'pdfHtml5',
                title: window.location.pathname
            },
            {
                extend: 'print',
                title: window.location.pathname
            }
         
        ],
    });
} );