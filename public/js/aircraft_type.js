$(document).ready(function() {
    $('#aircraftTypes-table').DataTable({
    	scrollX : true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"aircraftType/getJson",
        columns: [
            {data: 'iata_code', name: 'iata_code'},
            {data: 'aircraft_name', name: 'aircraft_name'},
            {data: 'mtow', name: 'mtow'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );