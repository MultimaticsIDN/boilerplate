$('#flightLegs-table').DataTable({
	scrollX : true,
    processing: true,
    serverSide: true,
    ajax: BaseURL+"flightLeg/getJson",
    columns: [
        {data: 'depart_for', name: 'depart_for'}, 
        {
            data: 'depart_at',
            "render": function (data) {
              // console.log("data",data);
              if (!isBlank(data)) {
                var d = new Date(data),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
              var str = month + "/" + day + "/" + year;
              return str;  
              }else{
                return "";
              }
            },
            name: 'depart_at'
        },
        {data: 'arrive_from', name: 'arrive_from'},  
        {
            data: 'arrive_at',
            "render": function (data) {
              // console.log("data",data);
              if (!isBlank(data)) {
                var d = new Date(data),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
              var str = month + "/" + day + "/" + year;
              return str;  
              }else{
                return "";
              }
            },
            name: 'arrive_at'
        },
        {data: 'action', name: 'action', orderable: false, searchable: false},
    ]
});