$(document).ready(function() {
    $('#countries-table').DataTable({
        scrollX: true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"countries/getJson",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'iso2', name: 'iso2'},
            {data: 'iso3', name: 'iso3'},
            {data: 'tld', name: 'tld'},
            {data: 'population', name: 'population'},
            {data: 'capital', name: 'capital'},
            {data: 'fips_code', name: 'fips_code'},
            {data: 'phone_prefix', name: 'phone_prefix'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
} );