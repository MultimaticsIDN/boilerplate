<?php
// use Session;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('countries.index');
});


Route::post('/login', 'AccessController@setLogin')->name('login');
Route::post('/logout', 'AccessController@getLogout');

Route::get('/register', 'AccessController@register');
Route::post('/register', 'AccessController@registerPost');

Route::get('/form', function () {
    return view('main_form');
}); 

// Route::group(['middleware' => 'roles'], function () {
    //
    # for dataTables
    Route::get('airports/getJson','AirportController@json'); 
    Route::get('countries/getJson','CountryController@json');
    Route::get('city/getJson','CityController@json');
    Route::get('/home', 'HomeController@index');

    Route::get('/form', function () {
        return view('main_form');
    });
    Route::resource('airports', 'AirportController');
    Route::resource('cities', 'CityController');
    Route::resource('countries', 'CountryController');
    Route::resource('roles', 'RolesController');
    Route::resource('branches', 'BranchController');
// });