<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('airports', 'AirportAPIController');
Route::post('airport/update/{id}', 'AirportAPIController@update');
Route::get('airport/showTimeZone/{id}', 'AirportAPIController@showTimeZone');
Route::resource('cities', 'CityAPIController');
Route::get('countries/autocomplete', 'CountryAPIController@autoComplete');
Route::resource('countries', 'CountryAPIController');
Route::resource('roles', 'RolesAPIController');
Route::resource('users', 'UsersAPIController');
Route::resource('crew_visas', 'CrewVisaAPIController');
Route::post('crew_visa/delete/{id}', 'CrewVisaAPIController@destroy');
Route::resource('crew_passports', 'CrewPassportAPIController');
Route::post('crew_passport/delete/{id}', 'CrewPassportAPIController@destroy');
