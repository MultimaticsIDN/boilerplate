@extends('layouts.app')
@section('css')
<link href="{{ URL::asset('fullcalendar/lib/main.css') }}" rel='stylesheet' />
<style>
.fc-timegrid-event .fc-event-main{
    color: black;
}
</style>
@endsection
@section('content')
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Dashboard</li>
        </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">

                                <div class="card-body">
                                @if(Session::has('id'))
                                    Selamat Datang {{ session('username') }}
                                @endif
                                <br>
                                <br>
                                <div id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>&nbsp;AC : </label>  
                                </div>
                                <div class="col-xs-6"> 
                                    <label id="ac"></label> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>&nbsp;ROUTE : </label> 
                                </div>
                                <div class="col-xs-6"> 
                                    <label id="route"></label> 
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-primary" id="btnAHAN">AHAN</a>
                            <a class="btn btn-primary" id="btnGENDEC">GENDEC</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
@endsection
@push('scripts')

<script src="{{ URL::asset('js/helper.js') }}"></script>
<script src="{{ URL::asset('fullcalendar/lib/main.js') }}"></script>
<script src="{{ URL::asset('js/home.js') }}"></script>

@endpush
