{!! Form::open(['route' => ['users.destroy', $id], 'method' => 'delete']) !!}
	@if(in_array(session('roles_name'), array(env('ROLE_ITADMIN'))))
		<div class='btn-group'>
		    <a href="{{ route('users.show', [$id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
		    <a href="{{ route('users.edit', [$id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
		    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
		</div>
	@else
		@if(Session::get('id') == $id)
			<div class='btn-group'>
			    <a href="{{ route('users.show', [$id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
			    <a href="{{ route('users.edit', [$id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
			    <!-- {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
			</div>
		@else
			<div class='btn-group'>
			    <a href="{{ route('users.show', [$id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
			    <!-- <a href="{{ route('users.edit', [$id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> -->
			    <!-- {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
			</div>
		@endif
	@endif
	
 {!! Form::close() !!}