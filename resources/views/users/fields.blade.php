<div class="row">
    <!-- Name Field -->
  <div class="form-group col-sm-6">
      {!! Form::label('name', 'Name:') !!}
      {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
  </div>
  <!-- Email Field -->
  <div class="form-group col-sm-6">
      {!! Form::label('email', 'Email:') !!}
      {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
  </div>

</div> 
<div class="row">
    <!-- Password Field -->
  <div class="form-group col-sm-6">
      {!! Form::label('password', 'Password:') !!}
      {!! Form::password('password', ['class' => 'form-control','id'=>'pw1','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
  </div>

  <!-- Konfirmasi Password Field -->
  <div class="form-group col-sm-6">
      {!! Form::label('password2', 'Konfirmasi Password:') !!}
      {!! Form::password('password2', ['class' => 'form-control','id'=>'pw2','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
  </div> 
</div>
<div class="row">
   <!-- Roles Id Field -->
  <div class="form-group col-sm-4">
      {!! Form::label('company_id', 'Company:') !!}
      <select name="company_id" class="form-control">
        <option value="">---Choose Company---</option>
        @foreach($company as $company)
          <option value="{{$company->company_id}}" @if(isset($users->company_id))@if($company->company_id == $users->company_id) selected @endif @endif>{{$company->name}}</option>
        @endforeach
      </select>
      <!-- {!! Form::number('roles_id', null, ['class' => 'form-control']) !!} -->
  </div>
    <!-- Roles Id Field -->
  <div class="form-group col-sm-4">
      {!! Form::label('roles_id', 'Roles:') !!}
      <select name="roles_id" class="form-control">
        @foreach($roles as $roles)
          <option value="{{$roles->roles_id}}" @if(isset($users->roles_id))@if($roles->roles_id == $users->roles_id) selected @endif @endif>{{$roles->roles_name}}</option>
        @endforeach
      </select>
      <!-- {!! Form::number('roles_id', null, ['class' => 'form-control']) !!} -->
  </div>

  <div class="form-group col-sm-4">
      {!! Form::label('branch_code', 'Branch:') !!}
      <select name="branch_code" class="form-control">
        @foreach($branch as $branch)
          <option value="{{$branch->branch_code}}" @if(isset($users->branch_code))@if($branch->branch_code == $users->branch_code) selected @endif @endif>{{$branch->name}}</option>
        @endforeach
      </select>
      <!-- {!! Form::number('roles_id', null, ['class' => 'form-control']) !!} -->
  </div>

  <!-- Email Verified At Field -->
  <!-- <div class="form-group col-sm-6">
      {!! Form::label('email_verified_at', 'Email Verified At:') !!}
      {!! Form::date('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}
  </div> -->
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-secondary">Cancel</a>
</div>
