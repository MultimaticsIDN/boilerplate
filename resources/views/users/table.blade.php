@section('css')
@include('layouts.datatables_css')
@endsection
<div class="table-responsive-sm">
    <table class="table table-striped" id="users-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@push('scripts')
<script src="{{ URL::asset('js/helper.js') }}"></script>
<script src="{{ URL::asset('js/users.js') }}"></script>
@include('layouts.datatables_js')
@endpush