<div class="row">
        <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        <input type="text" name="name" class="form-control" value="{{ $users->name }}" readonly>
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        <input type="text" name="name" class="form-control" value="{{ $users->email }}" readonly>
    </div>
</div>
<div class="row">
        <!-- Roles Id Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('roles_id', 'Roles:') !!}
        <input type="text" name="name" class="form-control" value="@if(isset($users->roles->roles_name)){{ $users->roles->roles_name }} @endif" readonly>
    </div>

    <div class="form-group col-sm-4">
        {!! Form::label('company_id', 'Company:') !!}
        <input type="text" name="company_id" class="form-control" value="@if(isset($users->company->name)){{ $users->company->name }} @endif" readonly>
    </div>

    <div class="form-group col-sm-4">
        {!! Form::label('branch_code', 'Branch:') !!}
        <input type="text" name="branch_code" class="form-control" value="@if(isset($users->branch->name)){{ $users->branch->name }} @endif" readonly>
    </div>

    <!-- Email Verified At Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('email_verified_at', 'Email Verified At:') !!}
        <input type="text" name="name" class="form-control" value="{{ $users->email_verified_at }}" readonly> 
    </div> -->
</div>

 