<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iso2', 'Iso2:') !!}
        {!! Form::text('iso2', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Iso3 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iso3', 'Iso3:') !!}
        {!! Form::text('iso3', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Tld Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tld', 'Tld:') !!}
        {!! Form::text('tld', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Population Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('population', 'Population:') !!}
        {!! Form::number('population', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Capital Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('capital', 'Capital:') !!}
        {!! Form::text('capital', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Fips Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fips_code', 'Fips Code:') !!}
        {!! Form::text('fips_code', null, ['class' => 'form-control','maxlength' => 2,'maxlength' => 2,'maxlength' => 2]) !!}
    </div>

    <!-- Phone Prefix Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_prefix', 'Phone Prefix:') !!}
        {!! Form::number('phone_prefix', null, ['class' => 'form-control']) !!}
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('countries.index') }}" class="btn btn-secondary">Cancel</a>
</div>
