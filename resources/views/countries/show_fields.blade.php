<div class= 'row'>
        <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        <input type="text" name="name" class="form-control" id="name"  value="@if(isset($country->name)){{ $country->name}}@endif" readonly> 
        <!-- <p>{{ $country->name }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iso2', 'Iso2:') !!}
        <input type="text" name="iso2" class="form-control" id="iso2"  value="@if(isset($country->iso2)){{ $country->iso2}}@endif" readonly>
        <!-- <p>{{ $country->iso2 }}</p> -->
    </div>
</div>

<div class='row'>
    <!-- Iso3 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iso3', 'Iso3:') !!}
        <input type="text" name="iso3" class="form-control" id="iso3"  value="@if(isset($country->iso3)){{ $country->iso3}}@endif" readonly>
        <!-- <p>{{ $country->iso3 }}</p> -->
    </div>

    <!-- Population Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('population', 'Population:') !!}
        <input type="text" name="population" class="form-control" id="population"  value="@if(isset($country->population)){{ $country->population}}@endif" readonly>
        <!-- <p>{{ $country->population }}</p> -->
    </div>

    <!-- Iso Numeric Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('iso_numeric', 'Iso Numeric:') !!}
        <p>{{ $country->iso_numeric }}</p>
    </div> -->
</div>





<div class='row'>
    <!-- Capital Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('capital', 'Capital:') !!}
        <input type="text" name="capital" class="form-control" id="capital"  value="@if(isset($country->capital)){{ $country->capital}}@endif" readonly>
        <!-- <p>{{ $country->capital }}</p> -->
    </div>

    <!-- Fips Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fips_code', 'Fips Code:') !!}
        <input type="text" name="fips_code" class="form-control" id="fips_code"  value="@if(isset($country->fips_code)){{ $country->fips_code}}@endif" readonly>
        <!-- <p>{{ $country->fips_code }}</p> -->
    </div>
</div>


<!-- <div class ='row'> -->
    <!-- Continent Id Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('continent_id', 'Continent Name:') !!}
        <input type="text" name="continent_id" class="form-control" id="continent_id"  value="@if(isset($country->continent_id)){{ $country->continent_id}}@endif" readonly>
        <p>{{ $country->continent_id }}</p>
    </div> -->

    <!-- Currency Id Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('currency_id', 'Currency Name:') !!}
        <input type="text" name="currency_id" class="form-control" id="currency_id"  value="@if(isset($country->currency_id)){{ $country->currency_id}}@endif" readonly>
        <p>{{ $country->currency_id }}</p>
    </div>
</div> -->



<div class = 'row'>
    <!-- Tld Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tld', 'Tld:') !!}
        <input type="text" name="tld" class="form-control" id="tld"  value="@if(isset($country->tld)){{ $country->tld}}@endif" readonly>
        <!-- <p>{{ $country->tld }}</p> -->
    </div>

    <!-- Phone Prefix Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_prefix', 'Phone Prefix:') !!}
        <input type="text" name="phone_prefix" class="form-control" id="phone_prefix"  value="@if(isset($country->phone_prefix)){{ $country->phone_prefix}}@endif" readonly>
        <!-- <p>{{ $country->phone_prefix }}</p> -->
    </div>
</div>


