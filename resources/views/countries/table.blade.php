@section('css')
@include('layouts.datatables_css')
@endsection
<div class="table-responsive-sm">
    <table class="table table-striped" id="countries-table" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Iso2</th>
                <th>Iso3</th>
                <th>Tld</th>
                <th>Population</th>
                <th>Capital</th>
                <th>Fips Code</th>
                <th>Phone Prefix</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@push('scripts')
<script src="{{ URL::asset('js/helper.js') }}"></script>
<script src="{{ URL::asset('js/country.js') }}"></script>
@include('layouts.datatables_js')
@endpush