<li class="nav-item {{ Request::is('airports*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('airports.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Airports</span>
    </a>
</li> 
<li class="nav-item {{ Request::is('countries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('countries.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Countries</span>
    </a>
</li>   

<li class="nav-item {{ Request::is('cities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('cities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Cities</span>
    </a>
</li>   