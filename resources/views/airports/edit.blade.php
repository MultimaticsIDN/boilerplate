@section('css')
<link rel="stylesheet" href="{{ URL::asset('css/all.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('airports.index') !!}">Airport</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Airport</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($airport, ['route' => ['airports.update', $airport->icao_code], 'method' => 'patch']) !!}

                              @include('airports.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
@push('scripts') 
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<script type="text/javascript"> 
   $('#timezone_id').select2({
        placeholder: 'Select a timezone'
    });  
   $('#city_id').select2({
        placeholder: 'Select a city'
    });  
   $('#country_id').select2({
        placeholder: 'Select a country'
    });   
</script>
<script src="{{ URL::asset('js/helper.js') }}"></script>
@endpush