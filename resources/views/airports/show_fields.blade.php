<div class='row'>
    <div class="form-group col-sm-6">
        {!! Form::label('icao_code', 'ICAO Code:') !!}
        <input type="text" name="icao_code" class="form-control" id="icao_code"  value="@if(isset($airport->icao_code)){{ $airport->icao_code}}@endif" readonly> 
        <!-- <p>{{ $airport->icao_code }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iata_code', 'IATA Code:') !!}
        <input type="text" name="iata_code" class="form-control" id="iata_code"  value="@if(isset($airport->iata_code)){{ $airport->iata_code}}@endif" readonly>
        <!-- <p>{{ $airport->iata_code }}</p> -->
    </div> 

    <div class="form-group col-sm-6">
        {!! Form::label('airport_name', 'Airport Name:') !!}
        <input type="text" name="airport_name" class="form-control" id="airport_name"  value="@if(isset($airport->airport_name)){{ $airport->airport_name}}@endif" readonly> 
        <!-- <p>{{ $airport->airport_name }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pcn', 'PCN:') !!}
        <input type="text" name="pcn" class="form-control" id="pcn"  value="@if(isset($airport->pcn)){{ $airport->pcn}}@endif" readonly>
        <!-- <p>{{ $airport->latitude }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('latitude', 'Latitude:') !!}
        <input type="text" name="latitude" class="form-control" id="latitude"  value="@if(isset($airport->latitude)){{ $airport->latitude}}@endif" readonly>
        <!-- <p>{{ $airport->latitude }}</p> -->
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('longitude', 'Longitude:') !!}
        <input type="text" name="longitude" class="form-control" id="longitude"  value="@if(isset($airport->longitude)){{ $airport->longitude}}@endif" readonly> 
        <!-- <p>{{ $airport->longitude }}</p> -->
    </div>


    <div class="form-group col-sm-6">
        {!! Form::label('runway_length', 'Runway Length:') !!}
        <input type="text" name="runway_length" class="form-control" id="runway_length"  value="@if(isset($airport->runway_length)){{ $airport->runway_length}}@endif" readonly> 
        <!-- <p>{{ $airport->runway_length }}</p> -->
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('geoname_id', 'Geoname Id:') !!}
        <input type="text" name="geoname_id" class="form-control" id="geoname_id"  value="@if(isset($airport->geoname_id)){{ $airport->geoname_id}}@endif" readonly> 
        <!-- <p>{{ $airport->geoname_id }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('operation_time', 'Operation Time:') !!}
        <input type="text" name="operation_time" class="form-control" id="operation_time"  value="@if(isset($airport->operation_time)){{ $airport->operation_time}}@endif" readonly>
        <!-- <p>{{ $airport->latitude }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_number', 'Phone Number:') !!}
        <input type="text" name="phone_number" class="form-control" id="phone_number"  value="@if(isset($airport->phone_number)){{ $airport->phone_number}}@endif" readonly>
        <!-- <p>{{ $airport->phone_number }}</p> -->
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('timezone_id', 'Country:') !!}
        <input type="text" name="timezone_id" class="form-control" id="timezone_id"  value="@if(isset($airport->country->name)){{ $airport->country->name}}@endif" readonly> 
        <!-- <p>{{ $airport->timezone_id }}</p> -->
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('timezone_id', 'Timezone:') !!}
        <input type="text" name="timezone_id" class="form-control" id="timezone_id"  value="@if(isset($airport->timezone->name)){{ $airport->timezone->name}}@endif" readonly> 
        <!-- <p>{{ $airport->timezone_id }}</p> -->
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('timezone_id', 'City:') !!}
        <input type="text" name="timezone_id" class="form-control" id="timezone_id"  value="@if(isset($airport->city->name)){{ $airport->city->name}}@endif" readonly> 
        <!-- <p>{{ $airport->timezone_id }}</p> -->
    </div>

    <!-- Iso2 Field -->
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('city_id', 'City Name:') !!}
        <input type="text" name="city_id" class="form-control" id="city_id"  value="@if(isset($airport->city_id)){{ $airport->city_id}}@endif" readonly>
        <p>{{ $airport->city_id }}</p>
    </div> -->
</div>