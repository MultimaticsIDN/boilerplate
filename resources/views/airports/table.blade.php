@section('css')
@include('layouts.datatables_css')
@endsection
<div class="table-responsive-sm">
    <table class="table table-striped" id="airports-table">
        <thead>
            <tr>
                <!-- <th>Country Id</th> -->
                <th>City </th>
                <th>Airport Name</th>
                <th>ICAO Code</th>
                <th>IATA Code</th>
                <th>PCN</th>
                <th>Runway Length</th> 
                <th>Operation Time</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody> </tbody>
    </table>
</div>
@push('scripts')
<script src="{{ URL::asset('js/helper.js') }}"></script>
<script src="{{ URL::asset('js/airport.js') }}"></script>
@include('layouts.datatables_js')
@endpush