<div class="row">
    <!-- Iata Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('icao_code', 'Icao Code:') !!}
        {!! Form::text('icao_code', null, ['class' => 'form-control','maxlength' => 4,'maxlength' => 4,'maxlength' => 4]) !!}
    </div>
     
    <!-- Iata Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('iata_code', 'Iata Code:') !!}
        {!! Form::text('iata_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Airport Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('airport_name', 'Airport Name:') !!}
        {!! Form::text('airport_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- PCN Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pcn', 'PCN:') !!}
        {!! Form::text('pcn', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Latitude Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('latitude', 'Latitude:') !!}
        {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Longitude Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('longitude', 'Longitude:') !!}
        {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
    </div>


    <!-- Runway Length Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('runway_length', 'Runway Length:') !!}
        {!! Form::number('runway_length', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Geoname Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('geoname_id', 'Geoname Id:') !!}
        {!! Form::number('geoname_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Operation Time Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('operation_time', 'Operation Time:') !!}
        {!! Form::text('operation_time', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>

    <!-- Phone Number Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone_number', 'Phone Number:') !!}
        {!! Form::text('phone_number', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
    </div>


    <!-- Iata Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('country_id', 'Country:') !!}
        <select id="country_id" name="country_id" class="form-control">
        <option value="" readonly>Select a country</option>
        @foreach($country as $country)
          <option value="{{$country->country_id}}" @if(isset($airport->country_id))@if($country->country_id == $airport->country_id) selected @endif @endif>{{$country->name}}</option>
        @endforeach
      </select>
    </div>


    <!-- Iata Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('timezone_id', 'Timezone:') !!}
        <select id="timezone_id" name="timezone_id" class="form-control">
        <option value="" readonly>Select a timezone</option>
        @foreach($timezone as $timezone)
          <option value="{{$timezone->timezone_id}}" @if(isset($airport->timezone_id))@if($timezone->timezone_id == $airport->timezone_id) selected @endif @endif>{{$timezone->name}}</option>
        @endforeach
      </select>
    </div>


    <!-- Iata Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('city_id', 'City:') !!}
        <select id="city_id" name="city_id" class="form-control">
        <option value="" readonly>Select a city</option>
        @foreach($city as $city)
          <option value="{{$city->city_id}}" @if(isset($airport->city_id))@if($city->city_id == $airport->city_id) selected @endif @endif>{{$city->name}}</option>
        @endforeach
      </select>
    </div>
</div> 
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('airports.index') }}" class="btn btn-secondary">Cancel</a>
</div>
