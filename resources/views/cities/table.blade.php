@section('css')
@include('layouts.datatables_css')
@endsection
<div class="table-responsive-sm">
    <table class="table table-striped" width="100%" id="cities-table">
        <thead>
            <tr>
                <th>Iata Code</th>
                <th>Name</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Timezone</th>
                <th>Action</th>
            </tr>
        </thead> 
    </table>
</div>

@push('scripts')
<script src="{{ URL::asset('js/helper.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#cities-table').DataTable({
        scrollX: true,
        processing: true,
        serverSide: true,
        ajax: BaseURL+"city/getJson",
        columns: [
            {data: 'iata_code', name: 'city.iata_code'},
            {data: 'name', name: 'city.name'},
            {data: 'latitude', name: 'city.latitude'},
            {data: 'longitude', name: 'city.longitude'},
            {data: 'timezone', name: 'timezone.name'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    } );
</script>
@include('layouts.datatables_js')
@endpush
