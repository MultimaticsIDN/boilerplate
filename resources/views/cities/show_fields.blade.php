<div class="row">
		<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('iata_code', 'Iata Code:') !!}
	     <input type="text" name="iata_code" value="@if(isset($city->iata_code)){{$city->iata_code}}@endif" class="form-control" readonly>
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('name', 'Name:') !!}
	     <input type="text" name="latitude" value="@if(isset($city->name)){{$city->name}}@endif" class="form-control" readonly>
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('latitude', 'Latitude:') !!}
	     <input type="text" name="latitude" value="@if(isset($city->latitude)){{$city->latitude}}@endif" class="form-control" readonly>
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('longitude', 'Longitude:') !!}
	     <input type="text" name="timezone" value="@if(isset($city->longitude)){{$city->longitude}}@endif" class="form-control" readonly>
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('timezone_id', 'timezone:') !!}
	     <input type="text" name="timezone" value="@if(isset($city->timezone->name)){{$city->timezone->name}}@endif" class="form-control" readonly>
	</div>

</div>