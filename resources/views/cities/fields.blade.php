<div class="row">
		<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('iata_code', 'Iata Code:') !!}
	    {!! Form::text('iata_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('name', 'Name:') !!}
	    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('latitude', 'Latitude:') !!}
	    {!! Form::text('latitude', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('longitude', 'Longitude:') !!}
	    {!! Form::text('longitude', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
	</div>

	<!-- Iata Code Field -->
	<div class="form-group col-sm-6">
	    {!! Form::label('timezone_id', 'timezone:') !!}
	    <select id="timezone_id" name="timezone_id" class="form-control">
        <option value="" readonly>Select a timezone</option>
        @foreach($timezone as $timezone)
          <option value="{{$timezone->timezone_id}}" @if(isset($city->timezone_id))@if($timezone->timezone_id == $city->timezone_id) selected @endif @endif>{{$timezone->name}}</option>
        @endforeach
      </select>
	</div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
