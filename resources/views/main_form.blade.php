@extends('layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
    </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
               @include('coreui-templates::common.errors')
               <div class="row">
                   <div class="col-lg-12">
                       <div class="card">
                           <div class="card-header">
                               <i class="fa fa-plus-square-o fa-lg"></i>
                               <strong>GROUND HANDLING REQUEST FORM</strong>
                           </div>

                           <div class="card-body">
                              <form>
                                <div class="row">
                                  <div class="col-lg-12">
                                  <fieldset class="border p-3">
                                    <legend  class="w-auto"> <h5>CONTACT INFORMATION</h5></legend>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">COMPANY</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="Company">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="Address" class="col-sm-2 col-form-label">ADDRESS</label>
                                        <div class="col-sm-10">
                                          <textarea class="form-control" id="Address" rows="1"></textarea>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="Invoice_Address" class="col-sm-2 col-form-label">INVOICE ADDRESS</label>
                                        <div class="col-sm-10">
                                          <textarea class="form-control" id="Invoice_Address" rows="1"></textarea>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="vat" class="col-sm-2 col-form-label">VAT NR</label>
                                        <div class="col-sm-10">
                                          <input type="vat" class="form-control" id="vat" placeholder="VAT NR">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="phone" class="col-sm-2 col-form-label">PHONE NBR</label>
                                        <div class="col-sm-10">
                                          <input type="phone" class="form-control" id="phone" placeholder="PHONE NBR">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="fax" class="col-sm-2 col-form-label">FAX NBR</label>
                                        <div class="col-sm-10">
                                          <input type="fax" class="form-control" id="fax" placeholder="FAX NBR">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="email" class="col-sm-2 col-form-label">E-MAIL</label>
                                        <div class="col-sm-10">
                                          <input type="email" class="form-control" id="email" placeholder="E-MAIL">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="info" class="col-sm-2 col-form-label">OTHER INFO</label>
                                        <div class="col-sm-10">
                                          <input type="info" class="form-control" id="info" placeholder="OTHER INFO">
                                        </div>
                                      </div>
                                  </fieldset>
                                </div>
                                </div>

                                <br>
                                 <!-- FLIGHT INFORMATION -->
                                <div class="row">
                                    <div class="col-lg-7">
                                        <fieldset class="border p-3">
                                        <legend  class="w-auto"> <h5>FLIGHT INFORMATION</h5></legend>
                                          <div class="form-group row">
                                            <label for="company" class="col-sm-2 col-form-label">AIRCRAFT REGISTRATION</label>
                                            <div class="col-sm-10">
                                              <input type="company" class="form-control" id="company" placeholder="">
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label for="Address" class="col-sm-2 col-form-label">AIRCRAFT TYPE (ICAO)</label>
                                            <div class="col-sm-10">
                                              <textarea class="form-control" id="Address" rows="1"></textarea>
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label for="Invoice_Address" class="col-sm-2 col-form-label">MTOW</label>
                                            <div class="col-sm-10">
                                              <textarea class="form-control" id="Invoice_Address" rows="1"></textarea>
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label for="vat" class="col-sm-2 col-form-label">TOTAL NUMBER OF SEAT</label>
                                            <div class="col-sm-10">
                                              <input type="vat" class="form-control" id="vat" placeholder="">
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label for="phone" class="col-sm-2 col-form-label">FLIGHT CATEGORY</label>
                                            <div class="col-sm-10">
                                              <input type="phone" class="form-control" id="phone" placeholder="">
                                            </div>
                                          </div>

                                      </fieldset>
                                    </div>
                                    <!-- FUELLING (TOTAL) -->
                                    <div class="col-lg-5">
                                    <fieldset class="border p-3">
                                      <legend  class="w-auto"> <h5>FUELLING (TOTAL)</h5></legend>
                                        <div class="form-group row">
                                          <label for="company" class="col-sm-2 col-form-label">AVGAS</label>
                                          <div class="col-sm-10">
                                            <input type="company" class="form-control" id="company" placeholder="">
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="Address" class="col-sm-2 col-form-label">JET A1</label>
                                          <div class="col-sm-10">
                                            <textarea class="form-control" id="Address" rows="1"></textarea>
                                          </div>
                                        </div>

                                    </fieldset>
                                    <br>
                                    <!-- CUSTOM REQUES -->
                                    <fieldset class="border p-3">
                                      <legend  class="w-auto"> <h5>CUSTOM REQUEST for Extra UE dep - fuelling</h5></legend>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">AVGAS</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="Address" class="col-sm-2 col-form-label">JET A1</label>
                                        <div class="col-sm-10">
                                          <textarea class="form-control" id="Address" rows="1"></textarea>
                                        </div>
                                      </div>


                                    </fieldset>
                                    </div>
                                </div>
                                <br>
                                <!-- SCHEDULE -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <fieldset class="border p-3">
                                        <legend  class="w-auto"> <h5>SCHEDULE</h5></legend>
                                        <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th scope="col">DATE</th>
                                            <th scope="col">FLT NR</th>
                                            <th scope="col">APT DEP</th>
                                            <th scope="col">ETD</th>
                                            <th scope="col">APT ARR</th>
                                            <th scope="col">ETA</th>
                                            <th scope="col">PAX NR</th>
                                            <th scope="col">CREW NR</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      </fieldset>
                                    </div>
                                </div>
                                <br>
                                <!-- FLIGHT STATUS -->
                                <div class="row">
                                  <div class="col-lg-6">
                                    <fieldset class="border p-3">
                                      <legend  class="w-auto"> <h5>FLIGHT STATUS</h5></legend>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">PRIVATE</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="Address" class="col-sm-2 col-form-label">COMMERCIAL (send AOC)</label>
                                        <div class="col-sm-10">
                                          <textarea class="form-control" id="Address" rows="1"></textarea>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">TRAINING</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">MILITARY</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">AMBULANCE</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>


                                    </fieldset>
                                  </div>
                                  <!-- OTHER SERVICES REQUESTED -->
                                  <div class="col-lg-6">
                                    <fieldset class="border p-3">
                                      <legend  class="w-auto"> <h5>OTHER SERVICES REQUESTED</h5></legend>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">WATER SERVICE</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="Address" class="col-sm-2 col-form-label">TOILETTE SERVICE</label>
                                        <div class="col-sm-10">
                                          <textarea class="form-control" id="Address" rows="1"></textarea>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">GPU</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">AIR STARTER UNIT</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">CLEANING</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">PASSENGER STEPS</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">METEO FOLDER</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="company" class="col-sm-2 col-form-label">OTHER</label>
                                        <div class="col-sm-10">
                                          <input type="company" class="form-control" id="company" placeholder="">
                                        </div>
                                      </div>


                                    </fieldset>
                                  </div>
                                </div>
                                <br>
                                <!-- CREW DETAILS -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <fieldset class="border p-3">
                                        <legend  class="w-auto"> <h5>CREW DETAILS</h5></legend>
                                        <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th scope="col">NAME</th>
                                            <th scope="col">D.O.B.</th>
                                            <th scope="col">PASSPORT NBR</th>
                                            <th scope="col">NATIONALITY</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                          </tr>
                                        </tbody>
                                      </table>
                                      </fieldset>
                                    </div>
                                </div>
                                <br>
                                <!-- PAX DETAILS -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <fieldset class="border p-3">
                                        <legend  class="w-auto"> <h5>PAX DETAILS</h5></legend>
                                        <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th scope="col">NAME</th>
                                            <th scope="col">D.O.B.</th>
                                            <th scope="col">PASSPORT NBR</th>
                                            <th scope="col">NATIONALITY</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                          </tr>
                                        </tbody>
                                      </table>
                                      </fieldset>
                                    </div>
                                </div>
                                <br>
                                <!-- CREDIT CARD DETAILS -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <fieldset class="border p-3">
                                        <legend  class="w-auto"> <h5>CREDIT CARD DETAILS</h5></legend>
                                        <div class="form-group row">
                                          <div class="col">
                                              <input type="text" class="form-control" placeholder="N">
                                            </div>
                                            <div class="col">
                                              <input type="text" class="form-control" placeholder="EXP">
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>
                                    </div>
                                </div>
                              </form>
                           </div>

                       </div>
                   </div>

               </div>
          </div>
   </div>
@endsection
