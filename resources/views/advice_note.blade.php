@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
    </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
               @include('coreui-templates::common.errors')
               <div class="row">
                   <div class="col-lg-12">
                       <div class="card">
                           <div class="card-header">
                               <i class="fa fa-plus-square-o fa-lg"></i>
                               <strong>Form</strong>
                           </div>

                           <div class="card-body">
                              {!! Form::open(['route' => 'flightCycles.store']) !!}
                                @csrf
                               <div class="form-group row">
                                 {!! Form::label('operator_name', 'Operator', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-10">
                                 {!! Form::text('operator_name', null, ['class' => 'form-control','id'=>'operator_name', 'onclick' => 'getListAutoComplete("operator_name","operator_id","operators")']) !!}
                                 {!! Form::hidden('operator_id', null, ["id"=>"operator_id"]) !!} <!-- Value -->
                                 <div id="empty-message" class="alert alert-danger" role="alert" style="margin-top:5px; display:none;">
                                   Please select item !
                                 </div>
                                 </div>
                               </div>
                               <div class="form-group row">
                                 {!! Form::label('address', 'Address', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-10">
                                 {!! Form::text('address', null, ['class' => 'form-control','id'=>'address']) !!}
                                 </div>
                               </div>
                               <div class="form-group row">
                                 {!! Form::label('aircraft_registration', 'Aircraft Registration', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-6">
                                 {!! Form::text('aircraft_registration', null, ['class' => 'form-control','id'=>'aircraft_registration']) !!}
                                 </div>
                                 {!! Form::label('flight_no', 'Flight No.', ['class' => 'col-sm-1 col-form-label']) !!}
                                 <div class="col-sm-3">
                                 {!! Form::number('flight_no', null, ['class' => 'form-control','id'=>'flight_no']) !!}
                                 </div>
                               </div>
                               <div class="form-group row">
                                 {!! Form::label('arrived_from', 'Arrived from', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-3">
                                 {!! Form::text('arrived_from', null, ['class' => 'form-control','id'=>'arrived_from']) !!}
                                 </div>
                                 {!! Form::label('arrived_at', 'at', ['class' => 'col-sm-1 col-form-label text-center']) !!}
                                 <div class="col-sm-2">
                                 {!! Form::text('arrived_at', null, ['class' => 'form-control','id'=>'arrived_at']) !!}
                                 </div>
                                 {!! Form::label('arrived_hours', 'Hours L.T. on', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-2">
                                 {!! Form::text('arrived_hours', null, ['class' => 'form-control','id'=>'arrived_hours']) !!}
                                 </div>
                               </div>
                               <div class="form-group row">
                                 {!! Form::label('departed_for', 'Departed for', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-3">
                                 {!! Form::text('departed_for', null, ['class' => 'form-control','id'=>'departed_for']) !!}
                                 </div>
                                 {!! Form::label('departed_at', 'at', ['class' => 'col-sm-1 col-form-label text-center']) !!}
                                 <div class="col-sm-2">
                                 {!! Form::text('departed_at', null, ['class' => 'form-control','id'=>'departed_at']) !!}
                                 </div>
                                 {!! Form::label('departed_hours', 'Hours L.T. on', ['class' => 'col-sm-2 col-form-label']) !!}
                                 <div class="col-sm-2">
                                 {!! Form::text('departed_hours', null, ['class' => 'form-control','id'=>'departed_hours']) !!}
                                 </div>
                               </div>
                               <div class="form-group row">
                                 <label for="id" class="col-sm-2 col-form-label">Service cycles</label>
                                 <div class="col-sm-2">
                                   <div class="form-check">
                                    <input class="form-check-input" name="serviceCycle[]" type="checkbox" value="" id="ARR-DEP">
                                    <label class="form-check-label" for="ARR-DEP">
                                      Arrival - Departure
                                    </label>
                                  </div>
                                  <div class="form-check">
                                   <input class="form-check-input" name="serviceCycle[]" type="checkbox" value="" id="DEP-ARR">
                                   <label class="form-check-label" for="DEP-ARR">
                                     Departure - Arrival
                                   </label>
                                 </div>
                                 </div>
                                 <div class="col-sm-2">
                                   <div class="form-check">
                                    <input class="form-check-input" name="serviceCycle[]" type="checkbox" value="" id="ARR">
                                    <label class="form-check-label" for="ARR">
                                      Arrival Only
                                    </label>
                                  </div>
                                  <div class="form-check">
                                   <input class="form-check-input" name="serviceCycle[]" type="checkbox" value="" id="DEP">
                                   <label class="form-check-label" for="DEP">
                                     Departure Only
                                   </label>
                                 </div>
                                 </div>

                               </div>
                               <div class="row">
                                  <div class="col text-right">
                                    <button type="button" type="submit" id="create" onclick="showTable()" class="btn btn-primary">Create Request</button>
                                  </div>
                               </div>
                              {!! Form::close() !!}


                               <br>
                               <div class="row">
                                  <div class="col-12" id="main_table" style="display:none">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <!-- <th rowspan="2" colspan="1"><input class='check_all' type='checkbox' /></th> -->
                                          <th rowspan="2" colspan="1" class="text-center" style="vertical-align : middle;">Services</th>
                                          <th rowspan="1" colspan="2" class="text-center">Departure</th>
                                          <th rowspan="1" colspan="2" class="text-center">Arrival</th>

                                        </tr>
                                        <tr>
                                          <!-- Departure -->
                                          <th class="text-center">Q</th>

                                          <th class="text-center">Remarks</th>
                                          <!-- Arrival -->
                                          <th class="text-center">Q</th>

                                          <th class="text-center">Remarks</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <!-- <tr>
                                          <td ><input type='checkbox' class='case'/></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                        </tr> -->

                                      </tbody>
                                    </table>
                                    <br>



                                  </div>
                                </div>




                           </div>

                       </div>
                   </div>

               </div>
          </div>
   </div>
@endsection
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::asset('js/helper.js') }}" charset="utf-8"></script>
<script src="{{ URL::asset('js/advice_note.js') }}" charset="utf-8"></script>
@endpush
