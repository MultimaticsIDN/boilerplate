<?php

namespace App\Http\Middleware;

use Redirect;
use Illuminate\Support\MessageBag;

use Closure;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!$request->session()->exists('id')) {
          $errors = new MessageBag(['email' => ['Access denied. You need to Login first'],'password' => ['Access denied. You need to Login first.']]);
          return redirect("login")->withErrors($errors)->withInput($request->except('password'));
          
          // return redirect('/login');
      }
      return $next($request);
    }
}
