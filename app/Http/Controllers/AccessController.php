<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Support\MessageBag;

use Illuminate\Http\Request;
use App\Models\Users;
// use App\Models\Roles;
// use App\Models\PenyimpananAdmin;

use Session;
use DB;
use Illuminate\Support\Facades\Hash;
class AccessController extends Controller
{

  public function setLogin(Request $request){

    $users = Users::leftJoin('roles','roles.roles_id','users.roles_id')->
    leftJoin('company','company.company_id','users.company_id')->
    select('users.id','users.name','users.password','users.branch_code','roles.roles_id','roles.roles_name','company.company_id','company.name as company_name')->
    where('users.email',$request->email)->first();
    // dd($request->password);
    if (!empty($users)) {
      if (Hash::check($request->password, $users->password)) {
        $request->session()->put('id', $users->id);
        $request->session()->put('username', $users->name);
        $request->session()->put('roles_id', $users->roles_id);
        $request->session()->put('roles_name', $users->roles_name);
        $request->session()->put('branch_code', $users->branch_code ? $users->branch_code : $users->branch_code = null);
        $request->session()->put('company_id', $users->company_id ? $users->company_id : $users->company_id = null);
        $request->session()->put('company_name', $users->company_name ? $users->company_name : $users->company_name = null);
        return redirect('home');
      }
    }
    $errors = new MessageBag(['email' => ['Email and/or password invalid.'],'password' => ['Email and/or password invalid.']]);
    return Redirect::back()->withErrors($errors)->withInput($request->except('password'));

  }

  public function register()
  {
    return view('auth.register');
  }

  public function registerPost(Request $request)
  {


    $cek = $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);


    $users = new Users;
    $users->name = $request->name;
    $users->email = $request->email;
    $users->roles_id = $request->roles_id;
    $users->password = Hash::make($request->password);
    $users->save();

    $request->session()->put('id', $users->id);
    $request->session()->put('username', $users->name);
    $request->session()->put('role', $users->roles->roles_name);

      return redirect('home');
  }

  public function getLogout(){
    Session()->forget('id');
    Session()->forget('username');
    Session()->forget('roles_id');
    Session()->forget('roles_name');
    Session()->forget('company_id');
    Session()->forget('company_name');
    Session()->forget('branch_code');
    return redirect('login');
  }
}
