<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Repositories\UsersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Roles;
use App\Models\Users;
use App\Models\Branch;
use App\Models\Company;
use Flash;
use Response;
use DataTables;

class UsersController extends AppBaseController
{
    /** @var  UsersRepository */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the Users.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->usersRepository->all();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new Users.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Roles::select('roles_id','roles_name')
                        ->orderBy('roles_name', 'asc')
                        ->get();
        $branch = Branch::select('branch_code','name')->get();
        $company = Company::select('company_id','name')->get();
        return view('users.create',[
           'roles' => $roles,
           'branch' => $branch,
           'company' => $company
        ]);
    }

    /**
     * Store a newly created Users in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($request->password); 
        $users = $this->usersRepository->create($input);

        Flash::success('Users saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }
         $roles = Roles::select('roles_id','roles_name')->get();
         $branch = Branch::select('branch_code','name')->get();
        $company = Company::select('company_id','name')->get();
        return view('users.edit',[
            'users' => $users,
            'roles' => $roles,
            'branch' => $branch,
            'company' => $company
        ]);
    }

    /**
     * Update the specified Users in storage.
     *
     * @param int $id
     * @param UpdateUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($request->password); 
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        $users = $this->usersRepository->update($input, $id);

        Flash::success('Users updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified Users from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        $this->usersRepository->delete($id);

        Flash::success('Users deleted successfully.');

        return redirect(route('users.index'));
    }

    public function json()
    {
        $data = Users::leftJoin('roles','roles.roles_id','users.roles_id')->
        select(['users.id','users.name','users.email','roles.roles_name'])->orderBy('users.name', 'asc');
        return Datatables::of($data)
        ->addColumn('action', 'users.dataTables_actions')
        ->rawColumns(['action'])
        ->make(true);
    }
}
