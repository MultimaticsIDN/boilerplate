<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAirportRequest;
use App\Http\Requests\UpdateAirportRequest;
use App\Repositories\AirportRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use DataTables;
use App\Models\Airport;
use App\Models\Country;
use App\Models\City;
use App\Models\Timezone;
class AirportController extends AppBaseController
{
    /** @var  AirportRepository */
    private $airportRepository;

    public function __construct(AirportRepository $airportRepo)
    {
        $this->airportRepository = $airportRepo;
    }

    /**
     * Display a listing of the Airport.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $airports = $this->airportRepository->all();

        return view('airports.index')
            ->with('airports', $airports);
    }

    /**
     * Show the form for creating a new Airport.
     *
     * @return Response
     */
    public function create()
    {
        $city = City::select('city_id','name')->get();
        $country = Country::select('country_id','name')->get();
        $timezone = Timezone::select('timezone_id','name')->get();
        return view('airports.create',[
            'city'=>$city,
            'country'=>$country,
            'timezone'=>$timezone
        ]);
    }

    /**
     * Store a newly created Airport in storage.
     *
     * @param CreateAirportRequest $request
     *
     * @return Response
     */
    public function store(CreateAirportRequest $request)
    {
        $input = $request->all();

        $airport = $this->airportRepository->create($input);

        Flash::success('Airport saved successfully.');

        return redirect(route('airports.index'));
    }

    /**
     * Display the specified Airport.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            Flash::error('Airport not found');

            return redirect(route('airports.index'));
        }

        return view('airports.show')->with('airport', $airport);
    }

    /**
     * Show the form for editing the specified Airport.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            Flash::error('Airport not found');

            return redirect(route('airports.index'));
        }
        $city = City::select('city_id','name')->get();
        $country = Country::select('country_id','name')->get();
        $timezone = Timezone::select('timezone_id','name')->get();
        return view('airports.edit',[
            'city'=>$city,
            'country'=>$country,
            'timezone'=>$timezone,
            'airport'=> $airport
        ]);
    }

    /**
     * Update the specified Airport in storage.
     *
     * @param int $id
     * @param UpdateAirportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAirportRequest $request)
    {
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            Flash::error('Airport not found');

            return redirect(route('airports.index'));
        }

        $airport = $this->airportRepository->update($request->all(), $id);

        Flash::success('Airport updated successfully.');

        return redirect(route('airports.index'));
    }

    /**
     * Remove the specified Airport from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            Flash::error('Airport not found');

            return redirect(route('airports.index'));
        }

        $this->airportRepository->delete($id);

        Flash::success('Airport deleted successfully.');

        return redirect(route('airports.index'));
    }

    public function json()
    {
        $data = Airport::leftjoin('city','airport.city_id','city.city_id')
                ->select(['airport.airport_name','airport.icao_code','airport.iata_code','airport.pcn','airport.runway_length','airport.operation_time','city.name as city'])
                ->orderBy('airport_name', 'asc'); 
        return Datatables::of($data)
        ->addColumn('action', 'airports.dataTables_actions')
        ->rawColumns(['action'])
        ->make(true);
    }
}
