<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAirportAPIRequest;
use App\Http\Requests\API\UpdateAirportAPIRequest;
use App\Models\Airport;
use App\Repositories\AirportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AirportController
 * @package App\Http\Controllers\API
 */

class AirportAPIController extends AppBaseController
{
    /** @var  AirportRepository */
    private $airportRepository;

    public function __construct(AirportRepository $airportRepo)
    {
        $this->airportRepository = $airportRepo;
    }

    /**
     * Display a listing of the Airport.
     * GET|HEAD /airports
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $airports = $this->airportRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
            
        );

        return $this->sendResponse($airports->toArray(), 'Airports retrieved successfully');
    }

    public function autoComplete(Request $request){
        $term = $request->input("term");

        $airports = Airport::select('icao_code','airport_name')-> 
                              where('airport_name','like', $term.'%')
                            ->orWhere('airport_name','like','%'.$term)
                            ->orWhere('airport_name','like','%'.$term.'%')
                            ->orWhere('icao_code',$term)
                            ->get();
         $output = [];
          foreach($airports as $key => $value){
            $output[$key] = [
              "id" => $value->icao_code ,
              "label" =>$value->icao_code." - ".$value->airport_name,
              "value" =>$value->icao_code." - ".$value->airport_name

            ];
          }

          return $output;
    }

    public function autoCompleteByCity(Request $request){
        $term = $request->input("term");

        $airports = Airport::
                            leftjoin('city','city.city_id','airport.city_id')->
                            select('airport.icao_code','airport.airport_name','city.name','airport.iata_code')-> 
                              where('airport.airport_name','like', $term.'%')
                            ->orWhere('airport.airport_name','like','%'.$term)
                            ->orWhere('airport.airport_name','like','%'.$term.'%')
                            ->orWhere('city.name','like', $term.'%')
                            ->orWhere('city.name','like','%'.$term)
                            ->orWhere('city.name','like','%'.$term.'%')
                            ->orWhere('airport.iata_code','like', $term.'%')
                            ->orWhere('airport.iata_code','like','%'.$term)
                            ->orWhere('airport.iata_code','like','%'.$term.'%')
                            ->orWhere('airport.icao_code',$term)
                            ->get();
         $output = [];
          foreach($airports as $key => $value){
            $output[$key] = [
              "id" => $value->icao_code ,
              "label" =>$value->name." - ".$value->iata_code." - ".$value->airport_name,
              "value" =>$value->name." - ".$value->iata_code." - ".$value->airport_name

            ];
          }

          return $output;
    }

    /**
     * Store a newly created Airport in storage.
     * POST /airports
     *
     * @param CreateAirportAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAirportAPIRequest $request)
    {
        $input = $request->all();

        $airport = $this->airportRepository->create($input);

        return $this->sendResponse($airport->toArray(), 'Airport saved successfully');
    }

    /**
     * Display the specified Airport.
     * GET|HEAD /airports/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Airport $airport */
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        return $this->sendResponse($airport->toArray(), 'Airport retrieved successfully');
    }

    public function showTimeZone($id)
    {
        /** @var Airport $airport */
        $airport = Airport::
                   leftjoin('timezone','timezone.timezone_id','airport.timezone_id')->
                   select('timezone.timezone_id','timezone.name','timezone.gmt')->
                   where('airport.icao_code',$id)->
                   first();

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        return $this->sendResponse($airport->toArray(), 'Airport retrieved successfully');
    }

    /**
     * Update the specified Airport in storage.
     * PUT/PATCH /airports/{id}
     *
     * @param int $id
     * @param UpdateAirportAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAirportAPIRequest $request)
    {
        $input = $request->all();

        /** @var Airport $airport */
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        $airport = $this->airportRepository->update($input, $id);

        return $this->sendResponse($airport->toArray(), 'Airport updated successfully');
    }

    /**
     * Remove the specified Airport from storage.
     * DELETE /airports/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Airport $airport */
        $airport = $this->airportRepository->find($id);

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        $airport->delete();

        return $this->sendSuccess('Airport deleted successfully');
    }


}
