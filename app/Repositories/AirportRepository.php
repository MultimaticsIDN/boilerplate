<?php

namespace App\Repositories;

use App\Models\Airport;
use App\Repositories\BaseRepository;

/**
 * Class AirportRepository
 * @package App\Repositories
 * @version December 19, 2020, 11:53 am UTC
*/

class AirportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'icao_code',
        'country_id',
        'iata_code',
        'airport_name',
        'latitude',
        'longitude',
        'pcn',
        'runway_length',
        'operation_time',
        'geoname_id',
        'phone_number',
        'timezone_id',
        'city_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Airport::class;
    }
}
