<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 * @package App\Models
 * @version September 20, 2020, 7:31 am UTC
 *
 * @property \App\Models\Timezone $timezone
 * @property \Illuminate\Database\Eloquent\Collection $airports
 * @property string $iata_code
 * @property string $name
 * @property number $latitude
 * @property number $longitude
 * @property integer $timezone_id
 */
class City extends Model
{
    use SoftDeletes;

    public $table = 'city';
    protected $primaryKey = 'city_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'city_id',
        'iata_code',
        'name',
        'latitude',
        'longitude',
        'timezone_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'city_id' => 'integer',
        'iata_code' => 'string',
        'name' => 'string',
        'latitude' => 'decimal:2',
        'longitude' => 'decimal:2',
        'timezone_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'city_id' => 'nullable|integer',
        'iata_code' => 'nullable|string|max:255',
        'name' => 'nullable|string|max:255',
        'latitude' => 'nullable|numeric',
        'longitude' => 'nullable|numeric',
        'timezone_id' => 'nullable|integer',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function timezone()
    {
        return $this->belongsTo(\App\Models\Timezone::class, 'timezone_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function airports()
    {
        return $this->hasMany(\App\Models\Airport::class, 'city_id');
    }
}
