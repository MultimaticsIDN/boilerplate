<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 * @package App\Models
 * @version September 20, 2020, 7:44 am UTC
 *
 * @property \App\Models\Continent $continent
 * @property \App\Models\Currency $currency
 * @property \Illuminate\Database\Eloquent\Collection $airports
 * @property \Illuminate\Database\Eloquent\Collection $crews
 * @property \Illuminate\Database\Eloquent\Collection $passengers
 * @property \Illuminate\Database\Eloquent\Collection $passenger1s
 * @property string $name
 * @property string $iso2
 * @property string $iso3
 * @property integer $iso_numeric
 * @property string $tld
 * @property integer $population
 * @property string $capital
 * @property integer $continent_id
 * @property integer $currency_id
 * @property string $fips_code
 * @property integer $phone_prefix
 */
class Country extends Model
{
    use SoftDeletes;

    public $table = 'country';
    protected $primaryKey = 'country_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'country_id',
        'name',
        'iso2',
        'iso3',
        // 'iso_numeric',
        'tld',
        'population',
        'capital',
        'continent_id',
        'currency_id',
        'fips_code',
        'phone_prefix'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'country_id' => 'integer',
        'name' => 'string',
        'iso2' => 'string',
        'iso3' => 'string',
        // 'iso_numeric' => 'integer',
        'tld' => 'string',
        'population' => 'integer',
        'capital' => 'string',
        'continent_id' => 'integer',
        'currency_id' => 'integer',
        'fips_code' => 'string',
        'phone_prefix' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'country_id' => 'nullable|integer',
        'name' => 'nullable|string|max:255',
        'iso2' => 'nullable|string|max:255',
        'iso3' => 'nullable|string|max:255',
        // 'iso_numeric' => 'nullable|integer',
        'tld' => 'nullable|string|max:255',
        'population' => 'nullable|integer',
        'capital' => 'nullable|string|max:255',
        'continent_id' => 'nullable|integer',
        'currency_id' => 'nullable|integer',
        'fips_code' => 'nullable|string|max:2',
        'phone_prefix' => 'nullable|integer',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function continent()
    {
        return $this->belongsTo(\App\Models\Continent::class, 'continent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function currency()
    {
        return $this->belongsTo(\App\Models\Currency::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function airports()
    {
        return $this->hasMany(\App\Models\Airport::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function crews()
    {
        return $this->hasMany(\App\Models\Crew::class, 'nationality');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function passengers()
    {
        return $this->hasMany(\App\Models\Passenger::class, 'nationality');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function passenger1s()
    {
        return $this->belongsToMany(\App\Models\Passenger::class, 'passenger_passport');
    }
}
