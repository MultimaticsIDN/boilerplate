<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Airport
 * @package App\Models
 * @version December 19, 2020, 11:53 am UTC
 *
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \App\Models\Timezone $timezone
 * @property \Illuminate\Database\Eloquent\Collection $flightContracts
 * @property \Illuminate\Database\Eloquent\Collection $flightLegs
 * @property \Illuminate\Database\Eloquent\Collection $flightLeg1s
 * @property integer $country_id
 * @property string $iata_code
 * @property string $airport_name
 * @property number $latitude
 * @property number $longitude
 * @property string $pcn
 * @property integer $runway_length
 * @property string $operation_time
 * @property integer $geoname_id
 * @property string $phone_number
 * @property integer $timezone_id
 * @property integer $city_id
 */
class Airport extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'icao_code';
    public $table = 'airport';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'icao_code',
        'country_id',
        'iata_code',
        'airport_name',
        'latitude',
        'longitude',
        'pcn',
        'runway_length',
        'operation_time',
        'geoname_id',
        'phone_number',
        'timezone_id',
        'city_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'icao_code' => 'string',
        'country_id' => 'integer',
        'iata_code' => 'string',
        'airport_name' => 'string',
        'latitude' => 'decimal:8',
        'longitude' => 'decimal:8',
        'pcn' => 'string',
        'runway_length' => 'integer',
        'operation_time' => 'string',
        'geoname_id' => 'integer',
        'phone_number' => 'string',
        'timezone_id' => 'integer',
        'city_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'country_id' => 'nullable|integer',
        'iata_code' => 'nullable|string|max:255',
        'airport_name' => 'nullable|string|max:255',
        'latitude' => 'nullable|numeric',
        'longitude' => 'nullable|numeric',
        'pcn' => 'nullable|string|max:50',
        'runway_length' => 'nullable|integer',
        'operation_time' => 'nullable|string|max:45',
        'geoname_id' => 'nullable|integer',
        'phone_number' => 'nullable|string|max:255',
        'timezone_id' => 'nullable|integer',
        'city_id' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function timezone()
    {
        return $this->belongsTo(\App\Models\Timezone::class, 'timezone_id');
    }
     /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flightContracts()
    {
        return $this->hasMany(\App\Models\FlightContract::class, 'aircraft_registration');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flightLegs()
    {
        return $this->hasMany(\App\Models\FlightLeg::class, 'arrive_from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flightLeg1s()
    {
        return $this->hasMany(\App\Models\FlightLeg::class, 'depart_for');
    }
}
