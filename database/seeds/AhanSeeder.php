<?php

use Illuminate\Database\Seeder;
// use Faker\Factory as Faker;
use App\Models\FlightLeg;
use App\Models\FlightCycle;
use App\Models\Aircraft;
use App\Models\Airport;

class AhanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $faker;
    public function __construct(){
          $this->faker = Faker\Factory::create();
    }
    public function run()
    {
        $getAircraftReg = $this->getAircraft_reg();
        $getAirportCode = $this->getAirport();
        $flag = ['Dep-Arr','Arr-Dep','Dep','Arr'];

        print("hacked by ~penimbunMasker \n");
        for ($i=1; $i <=1 ; $i++) {
            print($i);
            print(" ");

            $flight_numberRandom = $this->faker->numerify('#####');
            
            $data = new FlightCycle();
            $data->address = $this->faker->address();
            $data->aircraft_registration = $this->faker->randomElements($getAircraftReg)[0]->aircraft_registration;
            $data->flight_number = $flight_numberRandom;
            $data->arrived_from = $this->faker->randomElements($getAirportCode)[0]->icao_code;
            $data->depart_for = $this->faker->randomElements($getAirportCode)[0]->icao_code;
            $data->depart_at = $this->faker->date($format = 'Y-m-d', $max = 'now');
            $data->arrived_at = $this->faker->date($format = 'Y-m-d', $max = 'now');
            $data->etd = $this->faker->time($format = 'H:i', $max = 'now');
            $data->eta = $this->faker->time($format = 'H:i', $max = 'now');
            $data->atd = $this->faker->time($format = 'H:i', $max = 'now');
            $data->ata = $this->faker->time($format = 'H:i', $max = 'now');
            $data->leg = $this->faker->randomElements($flag)[0];
            $data->save();
            // dd($data);
            
            $seqId = [1,2];
            
            switch ($data->leg) {
                case 'Dep-Arr':
                    $this->createFlightLeg($seqId, $data);
                    break;
                case 'Arr-Dep':
                    $this->createFlightLeg($seqId, $data);
                    break;
                case 'Dep':
                    $this->createFlightLeg(1, $data);
                    break;
                case 'Arr':
                    $this->createFlightLeg(1, $data);
                    break;
                default:
                    break;
            }
           
        }
        //
    }

    public function getAircraft_reg()
    {
      $getAircraftReg = Aircraft::select('aircraft_registration')->get();
      
      return $getAircraftReg;
    }

    public function getAirport()
    {
      $getAirportCode = Airport::select('icao_code')->get();
      
      return $getAirportCode;
    }

    public function createFlightLeg($seqId ,$req)
    {
        if (is_array($seqId)) {
            foreach ($seqId as $key => $value) {
                $data = new FlightLeg();
                $data->flight_cycle_id = $req->flight_cycle_id;
                $data->sequence = $value;
                $data->save();
            }
        }else {
                $data = new FlightLeg();
                $data->flight_cycle_id = $req->flight_cycle_id;
                $data->sequence = $seqId;
                $data->save();
        }
        // dd($arr);
        
    }

}
