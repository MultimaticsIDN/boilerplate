<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Airport;
use Faker\Generator as Faker;

$factory->define(Airport::class, function (Faker $faker) {

    return [
        'country_id' => $faker->randomDigitNotNull,
        'iata_code' => $faker->word,
        'airport_name' => $faker->word,
        'latitude' => $faker->randomDigitNotNull,
        'longitude' => $faker->randomDigitNotNull,
        'geoname_id' => $faker->randomDigitNotNull,
        'phone_number' => $faker->word,
        'timezone_id' => $faker->randomDigitNotNull,
        'city_id' => $faker->randomDigitNotNull
    ];
});
