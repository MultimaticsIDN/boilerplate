<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'iso2' => $faker->word,
        'iso3' => $faker->word,
        'iso_numeric' => $faker->randomDigitNotNull,
        'tld' => $faker->word,
        'population' => $faker->randomDigitNotNull,
        'capital' => $faker->word,
        'continent_id' => $faker->randomDigitNotNull,
        'currency_id' => $faker->randomDigitNotNull,
        'fips_code' => $faker->word,
        'phone_prefix' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
