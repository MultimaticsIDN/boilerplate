<?php namespace Tests\Repositories;

use App\Models\Airport;
use App\Repositories\AirportRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AirportRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AirportRepository
     */
    protected $airportRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->airportRepo = \App::make(AirportRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_airport()
    {
        $airport = factory(Airport::class)->make()->toArray();

        $createdAirport = $this->airportRepo->create($airport);

        $createdAirport = $createdAirport->toArray();
        $this->assertArrayHasKey('id', $createdAirport);
        $this->assertNotNull($createdAirport['id'], 'Created Airport must have id specified');
        $this->assertNotNull(Airport::find($createdAirport['id']), 'Airport with given id must be in DB');
        $this->assertModelData($airport, $createdAirport);
    }

    /**
     * @test read
     */
    public function test_read_airport()
    {
        $airport = factory(Airport::class)->create();

        $dbAirport = $this->airportRepo->find($airport->id);

        $dbAirport = $dbAirport->toArray();
        $this->assertModelData($airport->toArray(), $dbAirport);
    }

    /**
     * @test update
     */
    public function test_update_airport()
    {
        $airport = factory(Airport::class)->create();
        $fakeAirport = factory(Airport::class)->make()->toArray();

        $updatedAirport = $this->airportRepo->update($fakeAirport, $airport->id);

        $this->assertModelData($fakeAirport, $updatedAirport->toArray());
        $dbAirport = $this->airportRepo->find($airport->id);
        $this->assertModelData($fakeAirport, $dbAirport->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_airport()
    {
        $airport = factory(Airport::class)->create();

        $resp = $this->airportRepo->delete($airport->id);

        $this->assertTrue($resp);
        $this->assertNull(Airport::find($airport->id), 'Airport should not exist in DB');
    }
}
