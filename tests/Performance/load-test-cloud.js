import {sleep} from 'k6';
import http from 'k6/http';

export let options = {
    duration: '15m',
    vus: 100,
    thresholds:{
        http_req_duration: ['p(95)<900'],
    }
};

export default function(){
    http.get('http://172.96.185.104/api/airports');
    sleep(4);
}
