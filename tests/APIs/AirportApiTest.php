<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Airport;

class AirportApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_airport()
    {
        $airport = factory(Airport::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/airports', $airport
        );

        $this->assertApiResponse($airport);
    }

    /**
     * @test
     */
    public function test_read_airport()
    {
        $airport = factory(Airport::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/airports/'.$airport->id
        );

        $this->assertApiResponse($airport->toArray());
    }

    /**
     * @test
     */
    public function test_update_airport()
    {
        $airport = factory(Airport::class)->create();
        $editedAirport = factory(Airport::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/airports/'.$airport->id,
            $editedAirport
        );

        $this->assertApiResponse($editedAirport);
    }

    /**
     * @test
     */
    public function test_delete_airport()
    {
        $airport = factory(Airport::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/airports/'.$airport->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/airports/'.$airport->id
        );

        $this->response->assertStatus(404);
    }
}
