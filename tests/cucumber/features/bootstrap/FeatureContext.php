<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use App\Utilities\Calculator;
/**
 * Defines application features from the specific context.
 */
// require_once __DIR__ . '\Calculator.php'; 

class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        if (!isset($this->calculator)) {
            $this->calculator = new Calculator();
        }
    }

    

    /**
     * @Given /^I have entered (\d+) into the calculator$/
     */
    public function iHaveEnteredIntoTheCalculator($argument1)
    {
        $this->calculator->push($argument1);
    }

    /**
     * @When /^I press add$/
     */
    public function iPressAdd()
    {
        $this->calculator->add();
    }

    /**
     * @Then /^the result should be (\d+) on the screen$/
     */
    public function theResultShouldBeOnTheScreen($argument1)
    {
        assert($argument1 == $this->calculator->result());
    }
}
